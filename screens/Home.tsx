import * as React from 'react';
import {Image, StyleSheet, Text, View, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import {Padding, Border, FontFamily, FontSize, Color} from '../GlobalStyles';

const Home = () => {
  const navigation = useNavigation();

  return (
    <View style={[styles.home, styles.homeLayout]}>
      <Image
        style={styles.star4x1Icon}
        resizeMode="cover"
        source={require('../assets/star4x-15.png')}
      />
      <View style={styles.parent}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye2.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        frame13Color="#e1bf89"
        propColor="#fff"
        propColor1="#fff"
        propColor2="#fff"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <View style={[styles.frameParent, styles.frameParentPosition]}>
        <Pressable
          style={[styles.bcMtLBiTarotParent, styles.parentShadowBox]}
          onPress={() => navigation.navigate('RtBi' as never)}>
          <Text style={[styles.bcMtL, styles.bcMtLTypo]}>
            Bốc một lá bài tarot
          </Text>
          <Text style={[styles.bitThngIp, styles.bitTypo]}>
            {'Để biết thông điệp từ vũ trụ gửi tới bạn '}
          </Text>
        </Pressable>
        <View style={[styles.chnMtCuHiParent, styles.parentShadowBox]}>
          <Text style={[styles.chnMtCu, styles.bcMtLTypo]}>
            Chọn một câu hỏi
          </Text>
          <Text style={[styles.bitThngIp1, styles.bitTypo]}>
            {'Để biết thông điệp từ vũ trụ gửi tới bạn '}
          </Text>
        </View>
      </View>
      <Pressable
        style={[styles.ngNhpContainer, styles.frameParentPosition]}
        onPress={() => navigation.navigate('TiKhon1' as never)}>
        <Text style={[styles.ngNhpTriNghimCcTn, styles.bcMtLTypo]}>
          Đăng nhập để trải nghiệm các tính năng thú vị
        </Text>
      </Pressable>
      <Pressable
        style={styles.iconusercircle}
        onPress={() => navigation.navigate('Setting' as never)}>
        <Image
          style={[styles.icon, styles.homeLayout]}
          resizeMode="cover"
          source={require('../assets/iconusercircle.png')}
        />
      </Pressable>
      <Image
        style={[styles.moon24x1Icon, styles.frameParentPosition]}
        resizeMode="cover"
        source={require('../assets/moon24x-1.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  homeLayout: {
    overflow: 'hidden',
    width: '100%',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  frameParentPosition: {
    left: '50%',
    position: 'absolute',
  },
  parentShadowBox: {
    paddingHorizontal: Padding.p_31xl,
    shadowOpacity: 1,
    elevation: 60,
    shadowRadius: 60,
    shadowOffset: {
      width: 0,
      height: 18,
    },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    borderRadius: Border.br_5xs,
    alignSelf: 'stretch',
    alignItems: 'center',
    paddingVertical: Padding.p_xs,
  },
  bcMtLTypo: {
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    textAlign: 'left',
  },
  bitTypo: {
    marginTop: 8,
    textAlign: 'center',
    fontFamily: FontFamily.body1,
    lineHeight: 17,
    fontSize: FontSize.body1_size,
    alignSelf: 'stretch',
  },
  star4x1Icon: {
    height: 738,
    opacity: 0.8,
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Padding.p_xs,
    flexDirection: 'row',
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  bcMtL: {
    color: Color.secondary1,
    fontSize: FontSize.heading3_size,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
  },
  bitThngIp: {
    color: Color.secondary1,
  },
  bcMtLBiTarotParent: {
    backgroundColor: Color.primary6,
  },
  chnMtCu: {
    fontSize: FontSize.heading3_size,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    color: Color.white,
  },
  bitThngIp1: {
    color: Color.white,
  },
  chnMtCuHiParent: {
    backgroundColor: '#656565',
    marginTop: 20,
  },
  frameParent: {
    marginLeft: -171.5,
    top: 401,
    width: 343,
    alignItems: 'center',
  },
  ngNhpTriNghimCcTn: {
    marginLeft: -153.5,
    fontSize: FontSize.size_mini,
    textDecoration: 'underline',
    lineHeight: 18,
    color: Color.white,
  },
  ngNhpContainer: {
    top: 674,
  },
  icon: {
    height: '100%',
  },
  iconusercircle: {
    left: 315,
    top: 62,
    width: 32,
    height: 32,
    position: 'absolute',
  },
  moon24x1Icon: {
    marginLeft: -118.5,
    top: 84,
    width: 237,
    height: 237,
    opacity: 0.8,
  },
  home: {
    backgroundColor: Color.secondary2,
    flex: 1,
    height: 812,
  },
});

export default Home;
