import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import LoginSection from '../components/LoginSection';
import BackSection from '../components/BackSection';
import {FontSize, FontFamily, Color, Padding} from '../GlobalStyles';

const Setting = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.setting}>
      <Image
        style={[styles.star4x1Icon, styles.parentPosition]}
        resizeMode="cover"
        source={require('../assets/star4x-14.png')}
      />
      <View style={[styles.parent, styles.parentPosition]}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        frame13Color="#fff"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <LoginSection />
      <BackSection
        trV="Người dùng"
        lock={require('../assets/lock.png')}
        propOpacity={0}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  parentPosition: {
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  star4x1Icon: {
    height: 744,
    opacity: 0.8,
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    color: Color.white,
    textAlign: 'left',
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  setting: {
    backgroundColor: Color.secondary2,
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default Setting;
