import * as React from 'react';
import {Image, StyleSheet, View, Text, ImageBackground} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import FrameComponent2 from '../components/FrameComponent2';
import BackSection from '../components/BackSection';
import {Border, FontSize, FontFamily, Color, Padding} from '../GlobalStyles';

const XemBiIcon = () => {
  const navigation = useNavigation();

  return (
    <ImageBackground
      style={styles.xemBiIcon}
      resizeMode="cover"
      source={require('../assets/xembi.png')}>
      <View style={styles.component2}>
        <Image
          style={styles.star4x1Icon}
          resizeMode="cover"
          source={require('../assets/star4x-1.png')}
        />
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <View style={[styles.component1, styles.parentFlexBox]}>
        <FrameComponent2
          ngunGcVLchSTarot="Tổng quan"
          propColor="#e1bf89"
          propDisplay="unset"
          propDisplay1="unset"
          propDisplay2="unset"
        />
        <View style={styles.iDinChoTnhNThunKhitParent}>
          <Text style={styles.iDinCho}>
            Đại diện cho tính nữ thuần khiết, tình mẫu tử, sự bao dung che chở
            của thiên nhiên với hình ảnh tượng trưng Mẹ Trái Đất hiền hòa. Lá
            bài này truyền tải những thông điệp tích cực, khuyến khích tạo sợi
            dây gắn bó thiêng liêng, bền chặt giữa con người với thiên nhiên.
            The Empress khuyên mỗi người cần biết ơn, hài lòng để có cuộc sống
            an yên, hạnh phúc.
          </Text>
          <View style={[styles.rectangleParent, styles.groupChildLayout]}>
            <View style={[styles.groupChild, styles.groupPosition]} />
            <View style={[styles.groupItem, styles.groupPosition]} />
          </View>
        </View>
      </View>
      <BackSection
        trV="The Empress"
        lock={require('../assets/lock.png')}
        propOpacity={0}
      />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    alignItems: 'center',
    width: 375,
    position: 'absolute',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  groupChildLayout: {
    height: 232,
    top: 0,
  },
  groupPosition: {
    borderRadius: Border.br_11xs,
    left: 5,
    position: 'absolute',
  },
  star4x1Icon: {
    marginLeft: -149,
    top: -40,
    width: 318,
    height: 490,
    opacity: 0.8,
    left: '50%',
    position: 'absolute',
  },
  component2: {
    marginLeft: -108.5,
    top: 90,
    width: 218,
    height: 390,
    left: '50%',
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    color: Color.white,
    textAlign: 'left',
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    left: 0,
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    justifyContent: 'space-between',
    flexDirection: 'row',
    top: 0,
    alignItems: 'center',
    width: 375,
  },
  iDinCho: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    color: Color.secondary1,
    zIndex: 0,
    textAlign: 'left',
    flex: 1,
  },
  groupChild: {
    width: 3,
    height: 232,
    top: 0,
    backgroundColor: Color.primary6,
  },
  groupItem: {
    marginTop: -115,
    top: '50%',
    right: -2,
    backgroundColor: Color.primary2,
    height: 27,
    transform: [
      {
        rotate: '180deg',
      },
    ],
  },
  rectangleParent: {
    left: 374,
    width: 8,
    zIndex: 1,
    position: 'absolute',
  },
  iDinChoTnhNThunKhitParent: {
    alignSelf: 'stretch',
    paddingHorizontal: Padding.p_base,
    paddingVertical: Padding.p_xl,
    flexDirection: 'row',
  },
  component1: {
    marginLeft: -187.5,
    top: 497,
    borderTopLeftRadius: Border.br_xs,
    borderTopRightRadius: Border.br_xs,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    elevation: 4,
    shadowOpacity: 1,
    height: 241,
    backgroundColor: Color.primary6,
    left: '50%',
    overflow: 'hidden',
  },
  xemBiIcon: {
    width: '100%',
    height: 812,
    overflow: 'hidden',
    flex: 1,
  },
});

export default XemBiIcon;
