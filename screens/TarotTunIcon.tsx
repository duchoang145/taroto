import React, {useState, useCallback} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Pressable,
  ImageBackground,
  Modal,
} from 'react-native';
import FrameComponent from '../components/FrameComponent';
import BackSection from '../components/BackSection';
import FrameComponent4 from '../components/FrameComponent4';
import {Color, FontSize, FontFamily, Padding} from '../GlobalStyles';

const TarotTunIcon = () => {
  const [asset14x2Visible, setAsset14x2Visible] = useState(false);
  const [asset14x3Visible, setAsset14x3Visible] = useState(false);
  const [asset14x31Visible, setAsset14x31Visible] = useState(false);
  const [asset14x32Visible, setAsset14x32Visible] = useState(false);
  const [asset14x33Visible, setAsset14x33Visible] = useState(false);
  const [asset14x34Visible, setAsset14x34Visible] = useState(false);
  const [asset14x35Visible, setAsset14x35Visible] = useState(false);

  const openAsset14x2 = useCallback(() => {
    setAsset14x2Visible(true);
  }, []);

  const closeAsset14x2 = useCallback(() => {
    setAsset14x2Visible(false);
  }, []);

  const openAsset14x3 = useCallback(() => {
    setAsset14x3Visible(true);
  }, []);

  const closeAsset14x3 = useCallback(() => {
    setAsset14x3Visible(false);
  }, []);

  const openAsset14x31 = useCallback(() => {
    setAsset14x31Visible(true);
  }, []);

  const closeAsset14x31 = useCallback(() => {
    setAsset14x31Visible(false);
  }, []);

  const openAsset14x32 = useCallback(() => {
    setAsset14x32Visible(true);
  }, []);

  const closeAsset14x32 = useCallback(() => {
    setAsset14x32Visible(false);
  }, []);

  const openAsset14x33 = useCallback(() => {
    setAsset14x33Visible(true);
  }, []);

  const closeAsset14x33 = useCallback(() => {
    setAsset14x33Visible(false);
  }, []);

  const openAsset14x34 = useCallback(() => {
    setAsset14x34Visible(true);
  }, []);

  const closeAsset14x34 = useCallback(() => {
    setAsset14x34Visible(false);
  }, []);

  const openAsset14x35 = useCallback(() => {
    setAsset14x35Visible(true);
  }, []);

  const closeAsset14x35 = useCallback(() => {
    setAsset14x35Visible(false);
  }, []);

  return (
    <>
      <ImageBackground
        style={styles.tarotTunIcon}
        resizeMode="cover"
        source={require('../assets/xembi.png')}>
        <View style={[styles.parent, styles.parentPosition]}>
          <Text style={styles.text}>9 : 00</Text>
          <View style={styles.iconsaxlinearbatteryemptyParent}>
            <Image
              style={styles.iconsaxlinearwifiLayout}
              resizeMode="cover"
              source={require('../assets/iconsaxlinearbatteryempty.png')}
            />
            <Image
              style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
              resizeMode="cover"
              source={require('../assets/iconsaxlinearwifi.png')}
            />
          </View>
        </View>
        <BackSection
          trV="Trở về"
          lock={require('../assets/lock.png')}
          propOpacity={0}
        />
        <View style={[styles.dBoTunTiCaBnParent, styles.parentPosition]}>
          <Text style={styles.chNhtTypo}>Dự báo tuần tới của bạn</Text>
          <Text style={[styles.text1, styles.text1SpaceBlock]}>
            23/5/2023 - 29/5/2023
          </Text>
        </View>
        <View style={[styles.frameGroup, styles.parentPosition]}>
          <FrameComponent4
            th2="Thứ 2"
            th3="Thứ 3"
            th4="Thứ 4"
            onAsset14x2Press={openAsset14x2}
            onAsset14x3Press={openAsset14x3}
            onAsset14x3Press1={openAsset14x31}
          />
          <FrameComponent4
            th2="Thứ 5"
            th3="Thứ 6"
            th4="Thứ 7"
            propMarginTop={14.74}
            onAsset14x2Press={openAsset14x32}
            onAsset14x3Press={openAsset14x33}
            onAsset14x3Press1={openAsset14x34}
          />
          <View style={styles.frameWrapper}>
            <View style={styles.iconsaxlinearbatteryemptyParent}>
              <View style={styles.asset14x3Parent}>
                <Pressable style={styles.asset14x3} onPress={openAsset14x35}>
                  <Image
                    style={styles.icon}
                    resizeMode="cover"
                    source={require('../assets/asset-14x-37.png')}
                  />
                </Pressable>
                <Text style={[styles.chNht, styles.chNhtTypo]}>Chủ nhật</Text>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>

      <Modal animationType="fade" transparent visible={asset14x2Visible}>
        <View style={styles.asset14x2Overlay}>
          <Pressable style={styles.asset14x2Bg} onPress={closeAsset14x2} />
          <FrameComponent onClose={closeAsset14x2} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x3Visible}>
        <View style={styles.asset14x3Overlay}>
          <Pressable style={styles.asset14x3Bg} onPress={closeAsset14x3} />
          <FrameComponent onClose={closeAsset14x3} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x31Visible}>
        <View style={styles.asset14x31Overlay}>
          <Pressable style={styles.asset14x31Bg} onPress={closeAsset14x31} />
          <FrameComponent onClose={closeAsset14x31} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x32Visible}>
        <View style={styles.asset14x32Overlay}>
          <Pressable style={styles.asset14x32Bg} onPress={closeAsset14x32} />
          <FrameComponent onClose={closeAsset14x32} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x33Visible}>
        <View style={styles.asset14x33Overlay}>
          <Pressable style={styles.asset14x33Bg} onPress={closeAsset14x33} />
          <FrameComponent onClose={closeAsset14x33} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x34Visible}>
        <View style={styles.asset14x34Overlay}>
          <Pressable style={styles.asset14x34Bg} onPress={closeAsset14x34} />
          <FrameComponent onClose={closeAsset14x34} />
        </View>
      </Modal>

      <Modal animationType="fade" transparent visible={asset14x35Visible}>
        <View style={styles.asset14x35Overlay}>
          <Pressable style={styles.asset14x35Bg} onPress={closeAsset14x35} />
          <FrameComponent onClose={closeAsset14x35} />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    justifyContent: 'space-between',
    width: 375,
    flexDirection: 'row',
  },
  text1SpaceBlock: {
    marginTop: 4,
    textAlign: 'left',
    color: Color.white,
  },
  parentPosition: {
    position: 'absolute',
    alignItems: 'center',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  chNhtTypo: {
    lineHeight: 17,
    fontSize: FontSize.body1_size,
    color: Color.primary2,
    textAlign: 'left',
    fontFamily: FontFamily.body1,
  },
  asset14x2Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x2Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x3Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x3Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x31Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x31Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x32Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x32Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x33Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x33Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x34Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x34Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    top: 0,
    left: 0,
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    justifyContent: 'space-between',
    width: 375,
    flexDirection: 'row',
  },
  text1: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
  },
  dBoTunTiCaBnParent: {
    top: 88,
    left: 109,
  },
  asset14x35Overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  asset14x35Bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  icon: {
    height: '100%',
    width: '100%',
  },
  asset14x3: {
    width: 192,
    height: 246,
  },
  chNht: {
    marginTop: 3.68,
  },
  asset14x3Parent: {
    alignItems: 'center',
  },
  frameWrapper: {
    marginTop: 14.74,
    alignItems: 'center',
  },
  frameGroup: {
    marginLeft: -171.5,
    top: 148,
    left: '50%',
    width: 343,
  },
  tarotTunIcon: {
    flex: 1,
    height: 812,
    overflow: 'hidden',
    width: '100%',
  },
});

export default TarotTunIcon;
