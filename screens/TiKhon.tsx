import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import BackSection from '../components/BackSection';
import RegistrationSection from '../components/RegistrationSection';
import FrameComponent3 from '../components/FrameComponent3';
import {FontSize, FontFamily, Color, Padding} from '../GlobalStyles';

const TiKhon = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.tiKhon}>
      <Image
        style={[styles.star4x1Icon, styles.parentPosition]}
        resizeMode="cover"
        source={require('../assets/star4x-18.png')}
      />
      <View style={[styles.parent, styles.parentPosition]}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <BackSection trV="Trở về" lock={require('../assets/lock2.png')} />
      <RegistrationSection />
      <FrameComponent3
        appleLogoBlack1={require('../assets/apple-logo-black-1.png')}
        facebook2={require('../assets/facebook-2.png')}
        search2={require('../assets/search-2.png')}
        chaCTiKhon="Chưa có tài khoản ? "
        ngKNgay="Đăng ký ngay"
        onFramePressablePress={() => navigation.navigate('TiKhon' as never)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  parentPosition: {
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  star4x1Icon: {
    height: 760,
    opacity: 0.8,
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    color: Color.white,
    textAlign: 'left',
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  tiKhon: {
    backgroundColor: Color.secondary2,
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default TiKhon;
