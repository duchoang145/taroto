import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import BackSection from '../components/BackSection';
import FrameComponent2 from '../components/FrameComponent2';
import {Padding, Border, FontSize, FontFamily, Color} from '../GlobalStyles';

const HcChiTit = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.hcChiTit}>
      <Image
        style={styles.star4x1Icon}
        resizeMode="cover"
        source={require('../assets/star4x-18.png')}
      />
      <View style={[styles.parent, styles.parentFlexBox]}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse2.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye3.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        frame13Color="#fff"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <BackSection
        trV="Nguồn gốc và lịch sử tarot"
        lock={require('../assets/lock.png')}
        propOpacity={0}
      />
      <Image
        style={[styles.tarotCard1Icon, styles.component1Position]}
        resizeMode="cover"
        source={require('../assets/tarotcard-11.png')}
      />
      <View style={[styles.component1, styles.component1Position]}>
        <FrameComponent2
          ngunGcVLchSTarot="Nguồn gốc và lịch sử tarot"
          propColor="#fff"
          propDisplay="none"
          propDisplay1="none"
          propDisplay2="none"
        />
        <View
          style={[styles.voKhongThK17TarotDeParent, styles.parentSpaceBlock]}>
          <Text style={styles.voKhongTh}>
            Vào khoảng Thế kỷ 17, Tarot de Marseille là một trong những dạng bài
            Tarot phổ biến nhất. Bộ bài thường được in bằng mộc bản và sau đó
            được tô màu bằng tay. Tuy nhiên, việc sử dụng các lá bài để bói toán
            đã xuất hiện từ sớm hơn vào Thế kỷ 14, bắt nguồn từ trò chơi bài
            Mamluk được truyền đến Tây Âu từ Thổ Nhĩ Kỳ.
          </Text>
          <View style={styles.rectangleParent}>
            <View style={[styles.groupChild, styles.groupLayout]} />
            <View style={[styles.groupItem, styles.groupLayout]} />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    alignItems: 'center',
    width: 375,
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  component1Position: {
    left: '50%',
    position: 'absolute',
  },
  parentSpaceBlock: {
    paddingVertical: Padding.p_xs,
    flexDirection: 'row',
  },
  groupLayout: {
    borderRadius: Border.br_11xs,
    position: 'absolute',
  },
  star4x1Icon: {
    height: 760,
    opacity: 0.8,
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    color: Color.white,
    textAlign: 'left',
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    justifyContent: 'space-between',
    paddingVertical: Padding.p_xs,
    flexDirection: 'row',
    left: 0,
    top: 0,
    alignItems: 'center',
    position: 'absolute',
  },
  tarotCard1Icon: {
    marginLeft: -29.5,
    top: 112,
    width: 60,
    height: 60,
  },
  voKhongTh: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    color: Color.secondary1,
    width: 343,
    zIndex: 0,
    textAlign: 'left',
  },
  groupChild: {
    top: 50,
    left: 3,
    width: 3,
    height: 232,
    backgroundColor: Color.primary6,
  },
  groupItem: {
    marginTop: -141,
    top: '50%',
    right: 5,
    backgroundColor: Color.primary2,
    height: 27,
    left: 0,
  },
  rectangleParent: {
    top: 19,
    left: 370,
    width: 6,
    height: 282,
    zIndex: 1,
    position: 'absolute',
  },
  voKhongThK17TarotDeParent: {
    height: 483,
    paddingHorizontal: Padding.p_base,
  },
  component1: {
    marginLeft: -187.5,
    top: 205,
    borderTopLeftRadius: Border.br_xs,
    borderTopRightRadius: Border.br_xs,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    elevation: 4,
    shadowOpacity: 1,
    height: 517,
    backgroundColor: Color.primary6,
    alignItems: 'center',
    width: 375,
    overflow: 'hidden',
  },
  hcChiTit: {
    backgroundColor: Color.secondary2,
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default HcChiTit;
