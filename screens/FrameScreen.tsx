import React, {useState, useCallback} from 'react';
import {Image, StyleSheet, View, Pressable, Modal} from 'react-native';
import FrameComponent from '../components/FrameComponent';

const FrameScreen = () => {
  const [frameContainerVisible, setFrameContainerVisible] = useState(false);

  const openFrameContainer = useCallback(() => {
    setFrameContainerVisible(true);
  }, []);

  const closeFrameContainer = useCallback(() => {
    setFrameContainerVisible(false);
  }, []);

  return (
    <>
      <Pressable style={styles.asset14x12Parent} onPress={openFrameContainer}>
        <Image
          style={styles.asset14xLayout}
          resizeMode="cover"
          source={require('../assets/asset-14x-12.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-11.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-101.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-91.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-81.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-71.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-61.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-51.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-41.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-31.png')}
        />
        <Image
          style={[styles.asset14x11, styles.asset14xLayout]}
          resizeMode="cover"
          source={require('../assets/asset-14x-28.png')}
        />
      </Pressable>

      <Modal animationType="fade" transparent visible={frameContainerVisible}>
        <View style={styles.frameContainerOverlay}>
          <Pressable
            style={styles.frameContainerBg}
            onPress={closeFrameContainer}
          />
          <FrameComponent onClose={closeFrameContainer} />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  asset14xLayout: {
    height: 123,
    width: 299,
  },
  frameContainerOverlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(113, 113, 113, 0.3)',
  },
  frameContainerBg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },
  asset14x11: {
    marginTop: -107.29,
  },
  asset14x12Parent: {
    shadowColor: 'rgba(225, 191, 137, 0.2)',
    shadowOffset: {
      width: 0,
      height: 24,
    },
    shadowRadius: 50,
    elevation: 50,
    shadowOpacity: 1,
    flex: 1,
    width: '100%',
    alignItems: 'center',
    transform: [
      {
        rotate: '177.2deg',
      },
    ],
  },
});

export default FrameScreen;
