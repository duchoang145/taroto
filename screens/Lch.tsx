import * as React from 'react';
import {Image, StyleSheet, Text, View, StatusBar} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import Section2022 from '../components/Section2022';
import {Padding, Color, FontSize, FontFamily} from '../GlobalStyles';

const Lch = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.lch}>
      <Image
        style={styles.star4x1Icon}
        resizeMode="cover"
        source={require('../assets/star4x-15.png')}
      />
      <View style={[styles.parent, styles.parentFlexBox]}>
        <Text style={[styles.text, styles.textFlexBox]}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse1.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye2.png')}
        iconCalendar={require('../assets/iconcalendar1.png')}
        frame13Color="#fff"
        propColor2="#e1bf89"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <StatusBar barStyle="default" />
      <Section2022 />
      <Text style={[styles.tarotia, styles.textFlexBox]}>Tarotia</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Padding.p_xs,
    flexDirection: 'row',
    width: 375,
    left: 0,
    position: 'absolute',
  },
  textFlexBox: {
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  star4x1Icon: {
    height: 738,
    opacity: 0.8,
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    top: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Padding.p_xs,
  },
  tarotia: {
    marginLeft: -98.5,
    top: 141,
    left: '50%',
    fontSize: 76,
    lineHeight: 90,
    fontFamily: FontFamily.sacramentoRegular,
    position: 'absolute',
    textAlign: 'left',
    color: Color.white,
  },
  lch: {
    backgroundColor: Color.secondary2,
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default Lch;
