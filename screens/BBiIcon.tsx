import * as React from 'react';
import {
  Text,
  StyleSheet,
  Image,
  View,
  Pressable,
  ImageBackground,
} from 'react-native';
import BackSection from '../components/BackSection';
import MajorArcanaSection from '../components/MajorArcanaSection';
import MinorArcanaSection from '../components/MinorArcanaSection';
import {useNavigation} from '@react-navigation/native';
import {Padding, FontSize, FontFamily, Color, Border} from '../GlobalStyles';

const BBiIcon = () => {
  const navigation = useNavigation();

  return (
    <ImageBackground
      style={styles.bBiIcon}
      resizeMode="cover"
      source={require('../assets/bbi.png')}>
      <View style={[styles.parent, styles.parentPosition]}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <BackSection trV="Trở về" lock={require('../assets/lock.png')} />
      <View style={styles.frameParent}>
        <View style={styles.frameGroup}>
          <View style={[styles.frameWrapper, styles.frameFlexBox]}>
            <View style={styles.iconcaretleftParent}>
              <Image
                style={styles.iconsaxlinearwifiLayout}
                resizeMode="cover"
                source={require('../assets/iconcaretleft.png')}
              />
              <View style={styles.mTWrapper}>
                <Text style={styles.mT}>Mô tả</Text>
              </View>
            </View>
          </View>
          <Text style={styles.iDinCho}>
            Đại diện cho tính nữ thuần khiết, tình mẫu tử, sự bao dung che chở
            của thiên nhiên với hình ảnh tượng trưng Mẹ Trái Đất hiền hòa. Lá
            bài này truyền tải những thông điệp tích cực, khuyến khích tạo sợi
            dây gắn bó thiêng liêng, bền chặt giữa con người với thiên nhiên.
            The Empress khuyên mỗi người cần biết ơn, hài lòng để có cuộc sống
            an yên, hạnh phúc.
          </Text>
        </View>
        <MajorArcanaSection />
        <MinorArcanaSection />
      </View>
      <View style={[styles.frameContainer, styles.frameFlexBox]}>
        <Pressable
          style={styles.iconhouseParent}
          onPress={() => navigation.navigate('Home' as never)}>
          <Image
            style={styles.iconhouse}
            resizeMode="cover"
            source={require('../assets/iconhouse1.png')}
          />
          <Text style={[styles.trangCh, styles.hcTypo]}>Trang chủ</Text>
        </Pressable>
        <Pressable
          style={styles.iconhouseParent}
          onPress={() => navigation.navigate('ChcNng' as never)}>
          <Image
            style={styles.iconhouse}
            resizeMode="cover"
            source={require('../assets/iconbook.png')}
          />
          <Text style={[styles.trangCh, styles.hcTypo]}>Xem</Text>
        </Pressable>
        <Pressable
          style={styles.iconhouseParent}
          onPress={() => navigation.navigate('Hc1' as never)}>
          <Image
            style={styles.iconhouse}
            resizeMode="cover"
            source={require('../assets/iconhandeye1.png')}
          />
          <Text style={[styles.hc, styles.hcTypo]}>Học</Text>
        </Pressable>
        <Pressable
          style={styles.iconhouseParent}
          onPress={() => navigation.navigate('Lch' as never)}>
          <Image
            style={styles.iconhouse}
            resizeMode="cover"
            source={require('../assets/iconcalendar.png')}
          />
          <Text style={[styles.trangCh, styles.hcTypo]}>Lịch</Text>
        </Pressable>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  parentPosition: {
    justifyContent: 'space-between',
    width: 375,
    left: 0,
    position: 'absolute',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  frameFlexBox: {
    paddingHorizontal: Padding.p_xl,
    alignItems: 'center',
    flexDirection: 'row',
  },
  hcTypo: {
    marginTop: 4,
    lineHeight: 12,
    fontSize: FontSize.body2_size,
    fontFamily: FontFamily.body1,
    textAlign: 'left',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    top: 0,
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    alignItems: 'center',
    flexDirection: 'row',
  },
  mT: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.secondary2,
    textAlign: 'left',
  },
  mTWrapper: {
    paddingVertical: Padding.p_3xs,
    marginLeft: 8,
    paddingHorizontal: 0,
    flexDirection: 'row',
  },
  iconcaretleftParent: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  frameWrapper: {
    borderRadius: Border.br_9xs,
    backgroundColor: Color.primary6,
    paddingVertical: 0,
    alignSelf: 'stretch',
  },
  iDinCho: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    marginTop: 20,
    fontFamily: FontFamily.body1,
    alignSelf: 'stretch',
    textAlign: 'left',
    color: Color.white,
  },
  frameGroup: {
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  frameParent: {
    top: 87,
    left: 16,
    width: 343,
    height: 829,
    paddingVertical: Padding.p_xl,
    paddingHorizontal: 0,
    position: 'absolute',
  },
  iconhouse: {
    width: 28,
    height: 28,
    overflow: 'hidden',
  },
  trangCh: {
    color: Color.white,
  },
  iconhouseParent: {
    width: 78,
    padding: Padding.p_3xs,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hc: {
    color: Color.primary2,
  },
  frameContainer: {
    bottom: 0,
    backgroundColor: Color.secondary2,
    height: 74,
    paddingVertical: Padding.p_5xs,
    justifyContent: 'space-between',
    width: 375,
    left: 0,
    position: 'absolute',
  },
  bBiIcon: {
    flex: 1,
    width: '100%',
    height: 807,
    overflow: 'hidden',
  },
});

export default BBiIcon;
