import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import BackSection from '../components/BackSection';
import {Color, FontSize, FontFamily, Padding, Border} from '../GlobalStyles';

const CiT = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.ciT}>
      <Image
        style={styles.star4x1Icon}
        resizeMode="cover"
        source={require('../assets/star4x-17.png')}
      />
      <View style={styles.parent}>
        <Text style={[styles.text, styles.textFlexBox]}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={[
              styles.iconsaxlinearbatteryempty,
              styles.component7ChildLayout,
            ]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.component7ChildLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse1.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye1.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        frame13Color="#fff"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <BackSection
        trV="Cài đặt"
        lock={require('../assets/lock.png')}
        propOpacity={0}
      />
      <View style={styles.frameParent}>
        <View style={styles.parentFlexBox}>
          <Text style={[styles.sound, styles.textFlexBox]}>Sound :</Text>
          <View style={styles.component7}>
            <Image
              style={styles.component7ChildLayout}
              resizeMode="cover"
              source={require('../assets/ellipse-1.png')}
            />
          </View>
        </View>
        <View style={[styles.notificationParent, styles.parentFlexBox]}>
          <Text style={[styles.sound, styles.textFlexBox]}>Notification :</Text>
          <View style={styles.component7}>
            <Image
              style={styles.component7ChildLayout}
              resizeMode="cover"
              source={require('../assets/ellipse-1.png')}
            />
          </View>
        </View>
        <View style={[styles.notificationParent, styles.parentFlexBox]}>
          <Text style={[styles.sound, styles.textFlexBox]}>English :</Text>
          <View style={styles.component7}>
            <Image
              style={styles.component7ChildLayout}
              resizeMode="cover"
              source={require('../assets/ellipse-1.png')}
            />
          </View>
        </View>
        <View style={[styles.notificationParent, styles.parentFlexBox]}>
          <Text style={[styles.sound, styles.textFlexBox]}>Dark mode :</Text>
          <View style={styles.component7}>
            <Image
              style={styles.component7ChildLayout}
              resizeMode="cover"
              source={require('../assets/ellipse-1.png')}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  textFlexBox: {
    textAlign: 'left',
    color: Color.white,
  },
  component7ChildLayout: {
    height: 20,
    width: 20,
  },
  parentFlexBox: {
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  star4x1Icon: {
    height: 745,
    opacity: 0.8,
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
  },
  iconsaxlinearbatteryempty: {
    overflow: 'hidden',
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
    overflow: 'hidden',
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  sound: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
  },
  component7: {
    borderRadius: Border.br_31xl,
    backgroundColor: Color.white,
    width: 46,
    padding: Padding.p_11xs,
    flexDirection: 'row',
  },
  notificationParent: {
    marginTop: 23,
  },
  frameParent: {
    marginTop: -181,
    marginLeft: -171.5,
    top: '50%',
    left: '50%',
    width: 343,
    paddingHorizontal: Padding.p_xl,
    paddingVertical: 30,
    alignItems: 'center',
    position: 'absolute',
  },
  ciT: {
    backgroundColor: Color.secondary2,
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default CiT;
