import * as React from 'react';
import {Image, StyleSheet, Text, View, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import BackSection from '../components/BackSection';
import {FontFamily, FontSize, Color, Padding, Border} from '../GlobalStyles';

const Setting1 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.setting}>
      <Image
        style={styles.star4x1Icon}
        resizeMode="cover"
        source={require('../assets/star4x-14.png')}
      />
      <View style={[styles.parent, styles.parentFlexBox]}>
        <Text style={styles.text}>9 : 00</Text>
        <View style={styles.iconsaxlinearbatteryemptyParent}>
          <Image
            style={styles.iconsaxlinearwifiLayout}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearbatteryempty.png')}
          />
          <Image
            style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
            resizeMode="cover"
            source={require('../assets/iconsaxlinearwifi.png')}
          />
        </View>
      </View>
      <FrameComponent1
        iconHouse={require('../assets/iconhouse.png')}
        iconBook={require('../assets/iconbook.png')}
        iconHandEye={require('../assets/iconhandeye.png')}
        iconCalendar={require('../assets/iconcalendar.png')}
        frame13Color="#fff"
        onFramePressablePress={() => navigation.navigate('Home' as never)}
        onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
        onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
        onFramePressablePress3={() => navigation.navigate('Lch' as never)}
      />
      <BackSection
        trV="Lịch sử"
        lock={require('../assets/lock.png')}
        propOpacity={0}
      />
      <View style={[styles.settingInner, styles.settingInnerPosition]}>
        <Pressable
          style={styles.frameWrapper}
          onPress={() => navigation.navigate('Setting1' as never)}>
          <View style={[styles.lchSParent, styles.parentFlexBox]}>
            <Text style={[styles.lchS, styles.readingTypo]}>Lịch sử</Text>
            <Image
              style={styles.iconcaretleft}
              resizeMode="cover"
              source={require('../assets/iconcaretleft2.png')}
            />
          </View>
        </Pressable>
      </View>
      <View style={[styles.reading1Parent, styles.settingInnerPosition]}>
        <Text style={[styles.reading1, styles.readingTypo]}>Reading 1</Text>
        <Text style={[styles.reading11, styles.readingTypo]}>Reading 1</Text>
        <Text style={[styles.reading11, styles.readingTypo]}>Reading 1</Text>
        <Text style={[styles.reading11, styles.readingTypo]}>Reading 1</Text>
        <Text style={[styles.reading11, styles.readingTypo]}>Reading 1</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  settingInnerPosition: {
    left: 16,
    position: 'absolute',
  },
  readingTypo: {
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    fontSize: FontSize.heading3_size,
    textAlign: 'left',
  },
  star4x1Icon: {
    height: 744,
    opacity: 0.8,
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    justifyContent: 'space-between',
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  lchS: {
    color: Color.secondary2,
  },
  iconcaretleft: {
    width: 16,
    height: 16,
    overflow: 'hidden',
  },
  lchSParent: {
    flex: 1,
  },
  frameWrapper: {
    borderRadius: Border.br_9xs,
    paddingHorizontal: Padding.p_xl,
    width: 343,
    alignItems: 'center',
    paddingVertical: Padding.p_xs,
    flexDirection: 'row',
  },
  settingInner: {
    top: 134,
    borderRadius: Border.br_5xs,
    backgroundColor: Color.primary6,
    width: 343,
  },
  reading1: {
    color: Color.white,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    fontSize: FontSize.heading3_size,
  },
  reading11: {
    marginTop: 24,
    color: Color.white,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    fontSize: FontSize.heading3_size,
  },
  reading1Parent: {
    top: 197,
  },
  setting: {
    backgroundColor: Color.secondary2,
    width: '100%',
    height: 812,
    overflow: 'hidden',
    flex: 1,
  },
});

export default Setting1;
