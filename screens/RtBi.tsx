import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import FrameComponent1 from '../components/FrameComponent1';
import BackSection from '../components/BackSection';
import {Color, FontSize, FontFamily, Padding} from '../GlobalStyles';
import XemBiIcon from './XemBiIcon';

const RtBi = () => {
  const navigation = useNavigation();
  const [enable, setEnable] = React.useState(true);
  React.useEffect(() => {
    setTimeout(() => {
      setEnable(false);
    }, 1000);
  });
  if (enable) {
    return (
      <View style={styles.rtBi}>
        <Image
          style={[styles.star4x1Icon, styles.parentPosition]}
          resizeMode="cover"
          source={require('../assets/star4x-15.png')}
        />
        <View style={[styles.parent, styles.parentPosition]}>
          <Text style={[styles.text, styles.textFlexBox]}>9 : 00</Text>
          <View style={styles.iconsaxlinearbatteryemptyParent}>
            <Image
              style={styles.iconsaxlinearwifiLayout}
              resizeMode="cover"
              source={require('../assets/iconsaxlinearbatteryempty.png')}
            />
            <Image
              style={[styles.iconsaxlinearwifi, styles.iconsaxlinearwifiLayout]}
              resizeMode="cover"
              source={require('../assets/iconsaxlinearwifi.png')}
            />
          </View>
        </View>
        <FrameComponent1
          iconHouse={require('../assets/iconhouse.png')}
          iconBook={require('../assets/iconbook.png')}
          iconHandEye={require('../assets/iconhandeye.png')}
          iconCalendar={require('../assets/iconcalendar.png')}
          onFramePressablePress={() => navigation.navigate('Home' as never)}
          onFramePressablePress1={() => navigation.navigate('ChcNng' as never)}
          onFramePressablePress2={() => navigation.navigate('Hc1' as never)}
          onFramePressablePress3={() => navigation.navigate('Lch' as never)}
        />
        <Image
          style={[styles.asset14x2, styles.hyNhmMtPosition]}
          resizeMode="cover"
          source={require('../assets/asset-14x-26.png')}
        />
        <View style={styles.asset14x2Parent}>
          <Image
            style={[styles.asset14x21, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-27.png')}
          />
          <Image
            style={[styles.asset14x3, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-3.png')}
          />
          <Image
            style={[styles.asset14x4, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-4.png')}
          />
          <Image
            style={[styles.asset14x5, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-5.png')}
          />
          <Image
            style={[styles.asset14x6, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-6.png')}
          />
          <Image
            style={[styles.asset14x7, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-7.png')}
          />
          <Image
            style={[styles.asset14x8, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-8.png')}
          />
          <Image
            style={[styles.asset14x9, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-9.png')}
          />
          <Image
            style={[styles.asset14x10, styles.asset14xLayout]}
            resizeMode="cover"
            source={require('../assets/asset-14x-10.png')}
          />
        </View>
        <BackSection
          trV="Trở về"
          lock={require('../assets/lock.png')}
          propOpacity={0}
        />
        <Text style={[styles.hyNhmMt, styles.hyNhmMtPosition]}>
          Hãy nhắm mắt lại trong 1 phút và nghĩ về 1 việc mà bạn cảm thấy yên
          bình nhất. Sau đó hãy rút 1 lá bài và đọc về tín hiệu từ vũ trụ gửi
          tới mình
        </Text>
      </View>
    );
  } else {
    return <XemBiIcon />;
  }
};

const styles = StyleSheet.create({
  parentPosition: {
    width: 375,
    left: 0,
    top: 0,
    position: 'absolute',
  },
  textFlexBox: {
    textAlign: 'left',
    color: Color.white,
  },
  iconsaxlinearwifiLayout: {
    height: 20,
    width: 20,
    overflow: 'hidden',
  },
  hyNhmMtPosition: {
    left: '50%',
    position: 'absolute',
  },
  asset14xLayout: {
    height: 123,
    width: 299,
    left: -4,
    position: 'absolute',
  },
  star4x1Icon: {
    height: 738,
    opacity: 0.8,
  },
  text: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.beVietnamProRegular,
  },
  iconsaxlinearwifi: {
    marginLeft: 16,
  },
  iconsaxlinearbatteryemptyParent: {
    flexDirection: 'row',
  },
  parent: {
    height: 38,
    paddingHorizontal: Padding.p_9xl,
    paddingVertical: Padding.p_xs,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  asset14x2: {
    marginLeft: -108.5,
    top: -500,
    width: 218,
    height: 308,
  },
  asset14x21: {
    top: 32,
  },
  asset14x3: {
    top: 28,
  },
  asset14x4: {
    top: 24,
  },
  asset14x5: {
    top: 20,
  },
  asset14x6: {
    top: 16,
  },
  asset14x7: {
    top: 12,
  },
  asset14x8: {
    top: 8,
  },
  asset14x9: {
    top: 4,
  },
  asset14x10: {
    top: 0,
    width: 299,
    left: -4,
  },
  asset14x2Parent: {
    top: 549,
    left: 41,
    shadowColor: 'rgba(225, 191, 137, 0.35)',
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowRadius: 50,
    elevation: 50,
    shadowOpacity: 1,
    width: 291,
    height: 147,
    position: 'absolute',
  },
  hyNhmMt: {
    marginLeft: -171.5,
    top: 348,
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    width: 343,
    textAlign: 'left',
    color: Color.white,
  },
  rtBi: {
    flex: 1,
    width: '100%',
    height: 812,
    overflow: 'hidden',
  },
});

export default RtBi;
