import React, {useEffect} from 'react';

import RNSplash from '@rn-base/splash';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {AppProvider} from '@rn-base/element';
import {StatusBar} from 'react-native';

const App = () => {
  useEffect(() => {
    setTimeout(() => {
      RNSplash.hide(1);
    }, 1500);
  }, []);

  // return (
  //   <>
  //     <NavigationContainer>
  //       {hideSplashScreen ? (
  //         <Stack.Navigator
  //           initialRouteName="Home"
  //           screenOptions={{headerShown: false}}>
  //           <Stack.Screen
  //             name="Home"
  //             component={Home}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Lch"
  //             component={Lch}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="ChcNng"
  //             component={ChcNng}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="TiKhon"
  //             component={TiKhon1}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="TiKhon1"
  //             component={TiKhon}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Hc"
  //             component={Hc1}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Hc1"
  //             component={Hc}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Setting"
  //             component={Setting}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="BBi"
  //             component={BBiIcon}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="TarotTun"
  //             component={TarotTunIcon}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Setting1"
  //             component={Setting1}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="CiT"
  //             component={CiT}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="HcChiTit"
  //             component={HcChiTit}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="RtBi"
  //             component={RtBi}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="Frame151"
  //             component={FrameScreen}
  //             options={{headerShown: false}}
  //           />
  //           <Stack.Screen
  //             name="XemBi"
  //             component={XemBiIcon}
  //             options={{headerShown: false}}
  //           />
  //         </Stack.Navigator>
  //       ) : (
  //         <FrameComponent />
  //       )}
  //     </NavigationContainer>
  //   </>
  // );

  return (
    <AppProvider
      colors={{
        dark: undefined,
        light: undefined,
        default: undefined,
      }}>
      <StatusBar barStyle="light-content" backgroundColor="transparent" />
      <SafeAreaProvider>
        <Navigator />
      </SafeAreaProvider>
    </AppProvider>
  );
};
export default App;
