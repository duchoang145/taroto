/* fonts */
export const FontFamily = {
  sacramentoRegular: 'Sacramento_regular',
  poppinsRegular: 'Poppins_regular',
  body1: 'Roboto_regular',
  beVietnamProRegular: 'Be Vietnam Pro_regular',
  heading3: 'Roboto_medium',
};
/* font sizes */
export const FontSize = {
  size_mid: 17,
  size_base_6: 16,
  body2_size: 10,
  size_xs: 12,
  body1_size: 14,
  heading3_size: 16,
  size_mini: 15,
  size_4xs_9: 9,
  size_7xs: 6,
  body3_size: 8,
};
/* Colors */
export const Color = {
  secondary2: '#2e2e30',
  white: '#fff',
  primary2: '#e1bf89',
  primary6: '#fff7e9',
  secondary6: '#8e8e8e',
  secondary1: '#252527',
  grey: '#d9d9d9',
  secondary5: '#666',
  secondary4: '#59595a',
};
/* Paddings */
export const Padding = {
  p_3xs: 10,
  p_xs: 12,
  p_xl: 20,
  p_5xs: 8,
  p_9xl: 28,
  p_21xl: 40,
  p_9xs: 4,
  p_7xs: 6,
  p_10xs: 3,
  p_base: 16,
  p_5xs_4: 7,
  p_11xs: 2,
  p_31xl: 50,
};
/* border radiuses */
export const Border = {
  br_10xs_8: 3,
  br_7xs_7: 6,
  br_5xs: 8,
  br_9xs: 4,
  br_31xl: 50,
  br_xs: 12,
  br_11xs: 2,
};
