import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {routes} from './routes';
import type {
  BottomTabNavigationParamList,
  MainStackScreenNavigationProps,
} from './types';

const Bottom = createBottomTabNavigator<BottomTabNavigationParamList>();

const BottomTab: React.FC<
  MainStackScreenNavigationProps<'main_stack_bottom_tab'>
> = () => {
  return (
    <Bottom.Navigator
      initialRouteName={routes.bt_home}
      screenOptions={{
        headerShown: false,
        lazy: true,
        tabBarActiveTintColor: colors.primary,
        tabBarLabelStyle: {
          marginTop: -10,
        },
      }}>
      <Bottom.Screen
        name={routes.bt_dns_setting}
        getComponent={() =>
          require('@features/SafeMobileApp/DNSSetting').default
        }
        options={{
          tabBarLabel: i18n.t('labels.bottom_tab.safezone'),
          tabBarIcon: ({focused}) =>
            focused ? <iconho /> : <SafeZoneOutlineIcon />,
        }}
      />
      <Bottom.Screen
        name={routes.bt_device}
        getComponent={() =>
          require('@features/SafeMobileApp/DeviceManager/Main').default
        }
        options={{
          tabBarLabel: i18n.t('labels.bottom_tab.devices'),
          tabBarIcon: ({focused}) =>
            focused ? <SafeKidFilledIcon /> : <SafeKidOutlineIcon />,
        }}
      />
      <Bottom.Screen
        name={routes.bt_notification}
        listeners={() => ({
          tabPress: () => fetch.current(),
        })}
        getComponent={() =>
          require('@features/SafeMobileApp/Notification').default
        }
        options={{
          tabBarLabel: i18n.t('labels.bottom_tab.notifications'),
          tabBarIcon: ({focused}) => (
            <NotificationIcon size={30} count={count} focused={focused} />
          ),
          unmountOnBlur: true,
        }}
      />
      <Bottom.Screen
        name={routes.bt_setting}
        getComponent={() => require('@features/SafeMobileApp/Setting').default}
        options={{
          tabBarLabel: i18n.t('labels.bottom_tab.menu'),
          tabBarIcon: ({focused}) =>
            focused ? <MenuFilledIcon /> : <MenuOutlineIcon />,
        }}
      />
    </Bottom.Navigator>
  );
};

export default BottomTab;
