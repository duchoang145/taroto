import type {RoutesType} from './types';

type Entries<T> = {
  [K in keyof T]: K;
};

export const routes: Entries<RoutesType> = {
  // bottom tab
  bt_calender: 'bt_calender',
  bt_home: 'bt_home',
  bt_study: 'bt_study',
  bt_watch: 'bt_watch',

  // auth

  // main
  main_stack_bottom_tab: 'main_stack_bottom_tab',
};
