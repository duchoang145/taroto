import type {NavigatorScreenParams, RouteProp} from '@react-navigation/core';
import type {NativeStackNavigationProp} from '../modules/@react-navigation/native-stack/src';

/**
 * Bottom Tab
 * @SafeMobile sm_a_bt${name}
 * @SafeMobileAgent sma_a_bt_${name}
 */

export type BaseBottomTabNavigationParamList = {
  bt_home: undefined;
  bt_watch: undefined;
  bt_study: undefined;
  bt_calender: undefined;
};

export type BottomTabNavigationParamList =
  BaseBottomTabNavigationParamList & {};

export interface BottomTabScreenNavigationProps<
  T extends keyof BottomTabNavigationParamList,
> {
  navigation: NativeStackNavigationProp<BottomTabNavigationParamList>;

  route: RouteProp<BottomTabNavigationParamList, T>;
}

/**
 * Auth Stack
 *
 */

export type BaseAuthNavigationParamList = {};

export type AuthStackNavigationParamList = BaseAuthNavigationParamList & {};

export interface AuthStackScreenNavigationProps<
  T extends keyof AuthStackNavigationParamList,
> {
  navigation: NativeStackNavigationProp<AuthStackNavigationParamList>;
  route: RouteProp<AuthStackNavigationParamList, T>;
}

// Main Stack

export type BaseMainStackNavigationParamList = {
  main_stack_bottom_tab: NavigatorScreenParams<BaseBottomTabNavigationParamList>;
};

export type MainStackNavigationParamList =
  BaseMainStackNavigationParamList & {};

export interface MainStackScreenNavigationProps<
  T extends keyof MainStackNavigationParamList,
> {
  navigation: NativeStackNavigationProp<MainStackNavigationParamList>;
  route: RouteProp<MainStackNavigationParamList, T>;
}

export type RoutesType = MainStackNavigationParamList &
  BottomTabNavigationParamList &
  AuthStackNavigationParamList;
