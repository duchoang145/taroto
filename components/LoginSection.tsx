import * as React from 'react';
import {Text, StyleSheet, Image, View, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import SettingsSection from './SettingsSection';
import {FontSize, FontFamily, Color, Border, Padding} from '../GlobalStyles';

const LoginSection = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.frameParent}>
      <Pressable
        style={styles.frameWrapper}
        onPress={() => navigation.navigate('TiKhon1' as never)}>
        <View style={[styles.frameContainer, styles.ngNhpParentFlexBox]}>
          <View style={[styles.ngNhpParent, styles.ngNhpParentFlexBox]}>
            <Text style={styles.ngNhp}>Đăng nhập</Text>
            <Image
              style={styles.iconcaretleft}
              resizeMode="cover"
              source={require('../assets/iconcaretleft1.png')}
            />
          </View>
        </View>
      </Pressable>
      <SettingsSection
        settingsText="Cài đặt"
        onFramePressablePress={() => navigation.navigate('BBi' as never)}
        onFramePressablePress1={() => navigation.navigate('CiT' as never)}
      />
      <SettingsSection
        settingsText="Lịch sử"
        onFramePressablePress={() => navigation.navigate('HcChiTit' as never)}
        onFramePressablePress1={() => navigation.navigate('Setting1' as never)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  ngNhpParentFlexBox: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ngNhp: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.secondary2,
    textAlign: 'left',
  },
  iconcaretleft: {
    width: 16,
    height: 16,
    overflow: 'hidden',
  },
  ngNhpParent: {
    flex: 1,
    justifyContent: 'space-between',
  },
  frameContainer: {
    borderRadius: Border.br_9xs,
    paddingHorizontal: Padding.p_xl,
    paddingVertical: Padding.p_xs,
    width: 343,
  },
  frameWrapper: {
    alignSelf: 'stretch',
    borderRadius: Border.br_5xs,
    backgroundColor: Color.primary6,
  },
  frameParent: {
    position: 'absolute',
    top: 225,
    left: 16,
    width: 343,
  },
});

export default LoginSection;
