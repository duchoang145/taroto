import React, {useMemo} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Pressable,
  ImageSourcePropType,
} from 'react-native';
import {FontFamily, FontSize, Color, Padding} from '../GlobalStyles';

type FrameComponent1Type = {
  iconHouse: ImageSourcePropType;
  iconBook: ImageSourcePropType;
  iconHandEye: ImageSourcePropType;
  iconCalendar: ImageSourcePropType;

  /** Style props */
  frame13Color?: string;
  propColor?: string;
  propColor1?: string;
  propColor2?: string;

  /** Action props */
  onFramePressablePress?: () => void;
  onFramePressablePress1?: () => void;
  onFramePressablePress2?: () => void;
  onFramePressablePress3?: () => void;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const FrameComponent1 = ({
  iconHouse,
  iconBook,
  iconHandEye,
  iconCalendar,
  frame13Color,
  propColor,
  propColor1,
  propColor2,
  onFramePressablePress,
  onFramePressablePress1,
  onFramePressablePress2,
  onFramePressablePress3,
}: FrameComponent1Type) => {
  const trangChStyle = useMemo(() => {
    return {
      ...getStyleValue('color', frame13Color),
    };
  }, [frame13Color]);

  const xemStyle = useMemo(() => {
    return {
      ...getStyleValue('color', propColor),
    };
  }, [propColor]);

  const hcStyle = useMemo(() => {
    return {
      ...getStyleValue('color', propColor1),
    };
  }, [propColor1]);

  const lchStyle = useMemo(() => {
    return {
      ...getStyleValue('color', propColor2),
    };
  }, [propColor2]);

  return (
    <View style={styles.frameParent}>
      <Pressable style={styles.iconhouseParent} onPress={onFramePressablePress}>
        <Image style={styles.iconhouse} resizeMode="cover" source={iconHouse} />
        <Text style={[styles.trangCh, styles.xemTypo, trangChStyle]}>
          Trang chủ
        </Text>
      </Pressable>
      <Pressable
        style={styles.iconhouseParent}
        onPress={onFramePressablePress1}>
        <Image style={styles.iconhouse} resizeMode="cover" source={iconBook} />
        <Text style={[styles.xem, styles.xemTypo, xemStyle]}>Xem</Text>
      </Pressable>
      <Pressable
        style={styles.iconhouseParent}
        onPress={onFramePressablePress2}>
        <Image
          style={styles.iconhouse}
          resizeMode="cover"
          source={iconHandEye}
        />
        <Text style={[styles.xem, styles.xemTypo, hcStyle]}>Học</Text>
      </Pressable>
      <Pressable
        style={styles.iconhouseParent}
        onPress={onFramePressablePress3}>
        <Image
          style={styles.iconhouse}
          resizeMode="cover"
          source={iconCalendar}
        />
        <Text style={[styles.xem, styles.xemTypo, lchStyle]}>Lịch</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  xemTypo: {
    marginTop: 4,
    textAlign: 'left',
    fontFamily: FontFamily.body1,
    lineHeight: 12,
    fontSize: FontSize.body2_size,
  },
  iconhouse: {
    width: 28,
    height: 28,
    overflow: 'hidden',
  },
  trangCh: {
    color: Color.primary2,
  },
  iconhouseParent: {
    width: 78,
    padding: Padding.p_3xs,
    justifyContent: 'center',
    alignItems: 'center',
  },
  xem: {
    color: Color.white,
  },
  frameParent: {
    position: 'absolute',
    marginLeft: -187.5,
    bottom: 0,
    left: '50%',
    backgroundColor: Color.secondary1,
    width: 375,
    height: 74,
    flexDirection: 'row',
    paddingHorizontal: Padding.p_xl,
    paddingVertical: Padding.p_5xs,
    justifyContent: 'space-between',
    alignItems: 'center',
    zIndex: 1000,
  },
});

export default FrameComponent1;
