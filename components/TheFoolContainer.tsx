import React, {useMemo} from 'react';
import {Image, StyleSheet, Text, View, ImageSourcePropType} from 'react-native';
import {Color, FontFamily} from '../GlobalStyles';

type TheFoolContainerType = {
  coordinates: ImageSourcePropType;
  coordinatesString: ImageSourcePropType;
  coordinatesId: ImageSourcePropType;

  /** Style props */
  propMarginTop?: number | string;
  propWidth?: number | string;
  propWidth1?: number | string;
  propWidth2?: number | string;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const TheFoolContainer = ({
  coordinates,
  coordinatesString,
  coordinatesId,
  propMarginTop,
  propWidth,
  propWidth1,
  propWidth2,
}: TheFoolContainerType) => {
  const frameView2Style = useMemo(() => {
    return {
      ...getStyleValue('marginTop', propMarginTop),
    };
  }, [propMarginTop]);

  const asset14x2Style = useMemo(() => {
    return {
      ...getStyleValue('width', propWidth),
    };
  }, [propWidth]);

  const asset14x21Style = useMemo(() => {
    return {
      ...getStyleValue('width', propWidth1),
    };
  }, [propWidth1]);

  const asset14x22Style = useMemo(() => {
    return {
      ...getStyleValue('width', propWidth2),
    };
  }, [propWidth2]);

  return (
    <View style={[styles.frameParent, frameView2Style]}>
      <View style={styles.asset14x2Parent}>
        <Image
          style={[styles.asset14x2, asset14x2Style]}
          resizeMode="cover"
          source={coordinates ?? ''}
        />
        <View style={styles.parent}>
          <Text style={[styles.text, styles.textTypo]}>#0</Text>
          <Text style={[styles.thePriestess, styles.textTypo]}>
            The Priestess
          </Text>
        </View>
      </View>
      <View style={styles.asset14x2Group}>
        <Image
          style={[styles.asset14x2, asset14x21Style]}
          resizeMode="cover"
          source={coordinatesString}
        />
        <View style={styles.parent}>
          <Text style={[styles.text, styles.textTypo]}>#1</Text>
          <Text style={[styles.thePriestess, styles.textTypo]}>
            The Magician
          </Text>
        </View>
      </View>
      <View style={styles.asset14x2Group}>
        <Image
          style={[styles.asset14x22, asset14x22Style]}
          resizeMode="cover"
          source={coordinatesId}
        />
        <View style={styles.parent}>
          <Text style={[styles.text, styles.textTypo]}>#2</Text>
          <Text style={[styles.thePriestess, styles.textTypo]}>The Fool</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  textTypo: {
    textAlign: 'left',
    color: Color.white,
    fontFamily: FontFamily.body1,
  },
  asset14x2: {
    width: 155,
    height: 141,
  },
  text: {
    fontSize: 6,
    lineHeight: 6,
  },
  thePriestess: {
    fontSize: 9,
    lineHeight: 10,
    marginTop: 2.97,
  },
  parent: {
    alignSelf: 'stretch',
    paddingHorizontal: 7,
    paddingVertical: 3,
    marginTop: 4,
    alignItems: 'center',
  },
  asset14x2Parent: {
    alignItems: 'center',
  },
  asset14x2Group: {
    marginLeft: 12,
    alignItems: 'center',
  },
  asset14x22: {
    width: 143,
    height: 141,
  },
  frameParent: {
    flexDirection: 'row',
  },
});

export default TheFoolContainer;
