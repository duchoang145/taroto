import * as React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {FontFamily, Color} from '../GlobalStyles';

const Section2022 = () => {
  return (
    <View style={styles.rectangleParent}>
      <View style={styles.groupChild} />
      <View style={styles.groupItem} />
      <Text style={[styles.text, styles.textTypo8]}>2022</Text>
      <Text style={[styles.mo, styles.textTypo7]}>Mo</Text>
      <Text style={[styles.fr, styles.textTypo6]}>Fr</Text>
      <Text style={[styles.we, styles.textTypo7]}>We</Text>
      <Text style={[styles.su, styles.suTypo]}>Su</Text>
      <Text style={[styles.text1, styles.textTypo6]}>16</Text>
      <Text style={[styles.text2, styles.textTypo6]}>11</Text>
      <Text style={[styles.text3, styles.textTypo6]}>23</Text>
      <Text style={[styles.text4, styles.textTypo6]}>30</Text>
      <Text style={[styles.text5, styles.textTypo5]}>20</Text>
      <Text style={[styles.text6, styles.textTypo5]}>13</Text>
      <Text style={[styles.text7, styles.textTypo5]}> 8</Text>
      <Text style={[styles.text8, styles.textTypo5]}>{'  '}</Text>
      <Text style={[styles.text9, styles.textTypo5]}>27</Text> 
      <Text style={[styles.text10, styles.textTypo7]}>19</Text>
      <Text style={[styles.text11, styles.textTypo7]}>12</Text>
      <Text style={[styles.text12, styles.textTypo4]}> 7</Text>
      <Text style={[styles.text13, styles.textTypo7]}>30</Text>
      <Text style={[styles.text14, styles.textTypo7]}>26</Text>
      <Text style={[styles.text15, styles.textTypo3]}>17</Text>
      <Text style={[styles.text16, styles.textTypo3]}>12</Text>
      <Text style={[styles.text17, styles.textTypo3]}> 5</Text>
      <Text style={[styles.text18, styles.textTypo3]}>24</Text>
      <Text style={[styles.text19, styles.textTypo3]}>31</Text>
      <Text style={[styles.text20, styles.textTypo2]}>21</Text>
      <Text style={[styles.text21, styles.textTypo2]}>14</Text>
      <Text style={[styles.text22, styles.textTypo2]}> 9</Text>
      <Text style={[styles.text23, styles.textTypo2]}> 2</Text>
      <Text style={[styles.text24, styles.textTypo5]}> 1</Text>
      <Text style={[styles.text25, styles.textTypo7]}> 4</Text> 
      <Text style={[styles.text26, styles.textTypo2]}>28</Text>
      <Text style={[styles.text27, styles.textTypo1]}>18</Text>
      <Text style={[styles.text28, styles.textTypo1]}>13</Text>
      <Text style={[styles.text29, styles.textTypo7]}> 6</Text>
      <Text style={[styles.text30, styles.suTypo]}>25</Text>
      <Text style={[styles.text31, styles.textTypo4]}>
        <Text style={styles.text32}> </Text>
        <Text style={styles.text33}>1</Text>
      </Text>
      <Text style={[styles.text34, styles.textTypo]}>22</Text>
      <Text style={[styles.text35, styles.textTypo]}>15</Text>
      <Text style={[styles.text36, styles.textTypo]}>10</Text>
      <Text style={[styles.text37, styles.textTypo]}> 3</Text>
      <Text style={[styles.text38, styles.textTypo]}>29</Text>
      <Text style={[styles.tu, styles.textTypo5]}>Tu</Text>
      <Text style={[styles.sa, styles.textTypo7]}>Sa</Text>
      <Text style={[styles.th, styles.textTypo]}>Th</Text>
      <Image
        style={[styles.groupInner, styles.groupInnerLayout]}
        resizeMode="cover"
        source={require('../assets/polygon-19.png')}
      />
      <Image
        style={[styles.polygonIcon, styles.groupInnerLayout]}
        resizeMode="cover"
        source={require('../assets/polygon-20.png')}
      />
      <Text style={[styles.december, styles.textTypo8]}>{'December '}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  textTypo8: {
    textAlign: 'left',
    fontFamily: FontFamily.poppinsRegular,
    fontSize: 17,
    color: Color.secondary2,
    position: 'absolute',
  },
  textTypo7: {
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo6: {
    left: '61.35%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  suTypo: {
    left: '86.45%',
    width: '6.37%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo5: {
    left: '22.31%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo4: {
    width: '4.78%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo3: {
    left: '74.1%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo2: {
    left: '35.46%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo1: {
    left: '86.85%',
    width: '5.98%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  textTypo: {
    left: '49%',
    fontSize: 16,
    height: '5.88%',
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.poppinsRegular,
    position: 'absolute',
  },
  groupInnerLayout: {
    maxHeight: '100%',
    overflow: 'hidden',
    maxWidth: '100%',
    position: 'absolute',
  },
  groupChild: {
    height: '100%',
    width: '100%',
    top: '0%',
    right: '0%',
    bottom: '0%',
    left: '0%',
    borderRadius: 6,
    backgroundColor: Color.primary6,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 1.4143426418304443,
    },
    shadowRadius: 5.66,
    elevation: 5.66,
    shadowOpacity: 1,
    position: 'absolute',
  },
  groupItem: {
    height: '8.4%',
    width: '9.16%',
    top: '47.48%',
    right: '57.37%',
    bottom: '44.12%',
    left: '33.47%',
    borderRadius: 3,
    backgroundColor: Color.primary2,
    position: 'absolute',
  },
  text: {
    height: '5.09%',
    width: '15.08%',
    top: '8.59%',
    left: '61.39%',
  },
  mo: {
    width: '7.97%',
    left: '9.56%',
    top: '22.69%',
    color: Color.secondary2,
  },
  fr: {
    width: '5.18%',
    top: '22.69%',
  },
  we: {
    width: '8.37%',
    left: '34.26%',
    top: '22.69%',
    color: Color.secondary2,
  },
  su: {
    top: '22.69%',
  },
  text1: {
    top: '62.18%',
    width: '5.58%',
  },
  text2: {
    top: '48.32%',
    width: '5.18%',
  },
  text3: {
    top: '75.63%',
    width: '6.77%',
  },
  text4: {
    top: '87.82%',
    width: '6.77%',
  },
  text5: {
    width: '5.98%',
    top: '75.63%',
  },
  text6: {
    width: '5.98%',
    top: '62.18%',
  },
  text7: {
    top: '48.32%',
    width: '5.18%',
  },
  text8: {
    top: '34.87%',
  },
  text9: {
    width: '5.98%',
    top: '87.82%',
  },
  text10: {
    width: '5.98%',
    top: '75.63%',
    left: '9.56%',
    color: Color.secondary2,
  },
  text11: {
    width: '5.98%',
    top: '62.18%',
    left: '9.56%',
    color: Color.secondary2,
  },
  text12: {
    top: '48.32%',
    left: '9.56%',
    color: Color.secondary2,
  },
  text13: {
    color: Color.primary2,
    top: '34.87%',
    width: '6.37%',
    left: '9.56%',
  },
  text14: {
    top: '87.82%',
    width: '6.37%',
    left: '9.56%',
    color: Color.secondary2,
  },
  text15: {
    width: '5.98%',
    top: '62.18%',
  },
  text16: {
    width: '5.98%',
    top: '48.32%',
  },
  text17: {
    top: '34.87%',
    width: '5.58%',
  },
  text18: {
    top: '75.63%',
    width: '6.77%',
  },
  text19: {
    width: '5.98%',
    top: '87.82%',
  },
  text20: {
    top: '75.63%',
    width: '5.58%',
    color: Color.secondary2,
  },
  text21: {
    top: '62.18%',
    width: '5.58%',
    color: Color.secondary2,
  },
  text22: {
    color: Color.primary6,
    top: '48.32%',
    width: '5.18%',
  },
  text23: {
    top: '34.87%',
    width: '6.37%',
    color: Color.secondary2,
  },
  text24: {
    top: '34.87%',
    width: '5.98%',
  },
  text25: {
    left: '59.76%',
    top: '34.87%',
    width: '6.77%',
    color: Color.secondary2,
  },
  text26: {
    top: '87.82%',
    width: '6.37%',
    color: Color.secondary2,
  },
  text27: {
    top: '62.18%',
  },
  text28: {
    top: '48.32%',
  },
  text29: {
    left: '87.65%',
    top: '34.87%',
    width: '5.18%',
    color: Color.secondary2,
  },
  text30: {
    top: '75.63%',
  },
  text32: {
    color: Color.secondary2,
  },
  text33: {
    color: Color.primary2,
  },
  text31: {
    left: '88.05%',
    top: '87.82%',
  },
  text34: {
    width: '5.98%',
    top: '75.63%',
  },
  text35: {
    top: '62.18%',
    width: '5.58%',
  },
  text36: {
    top: '48.32%',
    width: '5.58%',
  },
  text37: {
    width: '4.38%',
    top: '34.87%',
  },
  text38: {
    width: '5.98%',
    top: '87.82%',
  },
  tu: {
    width: '5.98%',
    top: '22.69%',
  },
  sa: {
    left: '73.71%',
    width: '6.37%',
    top: '22.69%',
    color: Color.secondary2,
  },
  th: {
    width: '5.98%',
    top: '22.69%',
  },
  groupInner: {
    height: '2.39%',
    width: '3.66%',
    top: '11.4%',
    right: '21.01%',
    bottom: '86.22%',
    left: '75.34%',
  },
  polygonIcon: {
    height: '3.18%',
    width: '4.22%',
    top: '10.5%',
    right: '42.75%',
    bottom: '86.32%',
    left: '53.02%',
  },
  december: {
    height: '7.14%',
    width: '28.69%',
    top: '8.4%',
    left: '24.7%',
  },
  rectangleParent: {
    height: '41.45%',
    width: '91.47%',
    top: '29.31%',
    right: '4.27%',
    bottom: '29.23%',
    left: '4.27%',
    position: 'absolute',
  },
});

export default Section2022;
