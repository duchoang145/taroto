import * as React from 'react';
import {Text, StyleSheet, Image, View, Pressable} from 'react-native';
import {FontSize, FontFamily, Color, Padding, Border} from '../GlobalStyles';

type SettingsSectionType = {
  settingsText?: string;

  /** Action props */
  onFramePressablePress?: () => void;
  onFramePressablePress1?: () => void;
};

const SettingsSection = ({
  settingsText,
  onFramePressablePress,
  onFramePressablePress1,
}: SettingsSectionType) => {
  return (
    <Pressable style={styles.frameWrapper} onPress={onFramePressablePress}>
      <Pressable
        style={[styles.frameContainer, styles.ciTParentFlexBox]}
        onPress={onFramePressablePress1}>
        <View style={[styles.ciTParent, styles.ciTParentFlexBox]}>
          <Text style={styles.ciT}>{settingsText}</Text>
          <Image
            style={styles.iconcaretleft}
            resizeMode="cover"
            source={require('../assets/iconcaretleft1.png')}
          />
        </View>
      </Pressable>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  ciTParentFlexBox: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ciT: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.secondary2,
    textAlign: 'left',
  },
  iconcaretleft: {
    width: 16,
    height: 16,
    overflow: 'hidden',
  },
  ciTParent: {
    flex: 1,
    justifyContent: 'space-between',
  },
  frameContainer: {
    width: 343,
    paddingHorizontal: Padding.p_xl,
    paddingVertical: Padding.p_xs,
    borderRadius: Border.br_9xs,
  },
  frameWrapper: {
    alignSelf: 'stretch',
    backgroundColor: Color.primary6,
    marginTop: 18,
    borderRadius: Border.br_9xs,
  },
});

export default SettingsSection;
