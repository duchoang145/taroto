import React, {useMemo} from 'react';
import {
  Image,
  StyleSheet,
  Pressable,
  Text,
  View,
  ImageSourcePropType,
} from 'react-native';
import {FontSize, FontFamily, Color} from '../GlobalStyles';

type FrameComponent4Type = {
  icon?: ImageSourcePropType;
  th2?: string;
  icon1?: ImageSourcePropType;
  th3?: string;
  icon2?: ImageSourcePropType;
  th4?: string;

  /** Style props */
  propMarginTop?: number | string;

  /** Action props */
  onAsset14x2Press?: () => void;
  onAsset14x3Press?: () => void;
  onAsset14x3Press1?: () => void;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const FrameComponent4 = ({
  icon,
  th2,
  icon1,
  th3,
  icon2,
  th4,
  propMarginTop,
  onAsset14x2Press,
  onAsset14x3Press,
  onAsset14x3Press1,
}: FrameComponent4Type) => {
  const frameView3Style = useMemo(() => {
    return {
      ...getStyleValue('marginTop', propMarginTop),
    };
  }, [propMarginTop]);

  return (
    <View style={[styles.frameParent, frameView3Style]}>
      <View style={styles.frameWrapper}>
        <View style={styles.frameContainer}>
          <View style={styles.frameWrapper}>
            <Pressable style={styles.asset14x2} onPress={onAsset14x2Press}>
              <Image
                style={styles.icon}
                resizeMode="cover"
                source={require('../assets/asset-14x-210.png')}
              />
            </Pressable>
            <Text style={styles.th2}>{th2}</Text>
          </View>
        </View>
      </View>
      <View style={styles.frameWrapper}>
        <View style={styles.frameContainer}>
          <View style={styles.frameWrapper}>
            <Pressable style={styles.asset14x3} onPress={onAsset14x3Press}>
              <Image
                style={styles.icon}
                resizeMode="cover"
                source={require('../assets/asset-14x-32.png')}
              />
            </Pressable>
            <Text style={styles.th2}>{th3}</Text>
          </View>
        </View>
      </View>
      <View style={styles.frameWrapper}>
        <View style={styles.frameContainer}>
          <View style={styles.frameWrapper}>
            <Pressable style={styles.asset14x31} onPress={onAsset14x3Press1}>
              <Image
                style={styles.icon}
                resizeMode="cover"
                source={require('../assets/asset-14x-33.png')}
              />
            </Pressable>
            <Text style={styles.th2}>{th4}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  icon: {
    width: '100%',
    height: '100%',
  },
  asset14x2: {
    width: 179,
    height: 246,
  },
  th2: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    color: Color.primary2,
    textAlign: 'left',
    marginTop: 3.68,
  },
  frameWrapper: {
    alignItems: 'center',
  },
  frameContainer: {
    flexDirection: 'row',
  },
  asset14x3: {
    width: 192,
    height: 246,
  },
  asset14x31: {
    width: 144,
    height: 246,
  },
  frameParent: {
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

export default FrameComponent4;
