import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import TheFoolContainer from './TheFoolContainer';
import {FontFamily, FontSize, Padding, Color, Border} from '../GlobalStyles';

const MinorArcanaSection = () => {
  return (
    <View style={styles.frameParent}>
      <View style={styles.frameWrapper}>
        <View style={styles.frameWrapper}>
          <View style={styles.frameContainer}>
            <View style={styles.iconcaretleftParent}>
              <Image
                style={styles.iconcaretleft}
                resizeMode="cover"
                source={require('../assets/iconcaretleft.png')}
              />
              <View style={styles.minorArcanaLNPhWrapper}>
                <Text style={styles.minorArcana}>Minor Arcana - Lá ẩn phụ</Text>
              </View>
            </View>
          </View>
          <View style={styles.frameView}>
            <View style={styles.wandsWrapper}>
              <Text style={[styles.wands, styles.cupsTypo]}>Wands</Text>
            </View>
            <View style={[styles.cupsWrapper, styles.wrapperBorder]}>
              <Text style={[styles.cups, styles.cupsTypo]}>Cups</Text>
            </View>
            <View style={[styles.swordWrapper, styles.wrapperBorder]}>
              <Text style={[styles.cups, styles.cupsTypo]}>Sword</Text>
            </View>
            <View style={styles.wrapperBorder}>
              <Text style={[styles.cups, styles.cupsTypo]}>Pentacles</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.frameParent1}>
        <TheFoolContainer
          coordinates={require('../assets/asset-14x-2.png')}
          coordinatesString={require('../assets/asset-14x-21.png')}
          coordinatesId={require('../assets/asset-14x-22.png')}
        />
        <TheFoolContainer
          coordinates={require('../assets/asset-14x-23.png')}
          coordinatesString={require('../assets/asset-14x-24.png')}
          coordinatesId={require('../assets/asset-14x-25.png')}
          propMarginTop={20}
          propWidth={80}
          propWidth1={80}
          propWidth2={80}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cupsTypo: {
    fontFamily: FontFamily.body1,
    lineHeight: 18,
    fontSize: FontSize.size_mini,
    textAlign: 'left',
  },
  wrapperBorder: {
    marginLeft: 6,
    borderColor: '#fff',
    paddingVertical: Padding.p_9xs,
    paddingHorizontal: Padding.p_xs,
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: Color.secondary4,
    flexDirection: 'row',
    borderRadius: Border.br_9xs,
  },
  iconcaretleft: {
    width: 20,
    height: 20,
    overflow: 'hidden',
  },
  minorArcana: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.secondary2,
    textAlign: 'left',
  },
  minorArcanaLNPhWrapper: {
    paddingHorizontal: 0,
    paddingVertical: Padding.p_3xs,
    marginLeft: 8,
    flexDirection: 'row',
  },
  iconcaretleftParent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  frameContainer: {
    backgroundColor: Color.primary6,
    width: 342,
    paddingHorizontal: Padding.p_xl,
    paddingVertical: 0,
    flexDirection: 'row',
    borderRadius: Border.br_9xs,
    alignItems: 'center',
  },
  wands: {
    color: Color.primary2,
  },
  wandsWrapper: {
    borderColor: '#e1bf89',
    width: 70,
    paddingVertical: Padding.p_9xs,
    paddingHorizontal: Padding.p_xs,
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: Color.secondary4,
    flexDirection: 'row',
    borderRadius: Border.br_9xs,
  },
  cups: {
    color: Color.white,
  },
  cupsWrapper: {
    width: 59,
  },
  swordWrapper: {
    width: 67,
  },
  frameView: {
    marginTop: 12,
    flexDirection: 'row',
  },
  frameWrapper: {
    alignItems: 'center',
  },
  frameParent1: {
    height: 201,
    marginTop: 20,
    alignItems: 'center',
  },
  frameParent: {
    marginTop: 30,
    alignItems: 'center',
  },
});

export default MinorArcanaSection;
