import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Color, FontFamily, FontSize, Padding, Border} from '../GlobalStyles';

const MajorArcanaSection = () => {
  return (
    <View style={[styles.frameParent, styles.parentFrameFlexBox]}>
      <View style={[styles.frameWrapper, styles.parentFrameFlexBox]}>
        <View style={styles.iconcaretleftParent}>
          <Image
            style={styles.iconcaretleft}
            resizeMode="cover"
            source={require('../assets/iconcaretleft.png')}
          />
          <View style={styles.majorArcanaLNChnhWrapper}>
            <Text style={styles.majorArcana}>Major Arcana - Lá ẩn chính</Text>
          </View>
        </View>
      </View>
      <View style={styles.frameGroup}>
        <View style={styles.star4x1Parent}>
          <Image
            style={[styles.star4x1Icon, styles.star4x1IconLayout]}
            resizeMode="cover"
            source={require('../assets/star4x-11.png')}
          />
          <View style={[styles.parent, styles.parentFrameFlexBox]}>
            <Text style={[styles.text, styles.textTypo]}>#0</Text>
            <Text style={[styles.thePriestess, styles.textTypo]}>
              The Priestess
            </Text>
          </View>
        </View>
        <View style={styles.star4x1Group}>
          <Image
            style={[styles.star4x1Icon1, styles.star4x1IconLayout]}
            resizeMode="cover"
            source={require('../assets/star4x-12.png')}
          />
          <View style={[styles.parent, styles.parentFrameFlexBox]}>
            <Text style={[styles.text, styles.textTypo]}>#1</Text>
            <Text style={[styles.thePriestess, styles.textTypo]}>
              The Magician
            </Text>
          </View>
        </View>
        <View style={styles.star4x1Group}>
          <Image
            style={[styles.star4x1Icon2, styles.star4x1IconLayout]}
            resizeMode="cover"
            source={require('../assets/star4x-13.png')}
          />
          <View style={[styles.parent, styles.parentFrameFlexBox]}>
            <Text style={[styles.text, styles.textTypo]}>#2</Text>
            <Text style={[styles.thePriestess, styles.textTypo]}>The Fool</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  parentFrameFlexBox: {
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  star4x1IconLayout: {
    opacity: 0.8,
    height: 296,
  },
  textTypo: {
    color: Color.white,
    fontFamily: FontFamily.body1,
    textAlign: 'left',
  },
  iconcaretleft: {
    width: 20,
    height: 20,
    overflow: 'hidden',
  },
  majorArcana: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.secondary2,
    textAlign: 'left',
  },
  majorArcanaLNChnhWrapper: {
    paddingHorizontal: 0,
    paddingVertical: Padding.p_3xs,
    marginLeft: 8,
    flexDirection: 'row',
  },
  iconcaretleftParent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  frameWrapper: {
    borderRadius: Border.br_9xs,
    backgroundColor: Color.primary6,
    paddingHorizontal: Padding.p_xl,
    paddingVertical: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  star4x1Icon: {
    width: 165,
  },
  text: {
    fontSize: FontSize.body3_size,
    lineHeight: 10,
  },
  thePriestess: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    marginTop: 4,
  },
  parent: {
    paddingHorizontal: Padding.p_3xs,
    paddingVertical: Padding.p_9xs,
    marginTop: 4,
    alignItems: 'center',
  },
  star4x1Parent: {
    alignItems: 'center',
  },
  star4x1Icon1: {
    width: 209,
  },
  star4x1Group: {
    marginLeft: 12,
    alignItems: 'center',
  },
  star4x1Icon2: {
    width: 145,
  },
  frameGroup: {
    marginTop: 20,
    flexDirection: 'row',
  },
  frameParent: {
    marginTop: 30,
    alignItems: 'center',
  },
});

export default MajorArcanaSection;
