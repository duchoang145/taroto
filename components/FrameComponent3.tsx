import React, {useMemo} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Pressable,
  ImageSourcePropType,
} from 'react-native';
import {FontSize, FontFamily, Color, Padding} from '../GlobalStyles';

type FrameComponent3Type = {
  appleLogoBlack1: ImageSourcePropType;
  facebook2: ImageSourcePropType;
  search2: ImageSourcePropType;
  chaCTiKhon?: string;
  ngKNgay?: string;

  /** Style props */
  propAlignSelf?: string;
  propHeight?: number | string;
  propMarginLeft?: number | string;
  propHeight1?: number | string;
  propMarginLeft1?: number | string;

  /** Action props */
  onFramePressablePress?: () => void;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const FrameComponent3 = ({
  appleLogoBlack1,
  facebook2,
  search2,
  chaCTiKhon,
  ngKNgay,
  propAlignSelf,
  propHeight,
  propMarginLeft,
  propHeight1,
  propMarginLeft1,
  onFramePressablePress,
}: FrameComponent3Type) => {
  const frameViewStyle = useMemo(() => {
    return {
      ...getStyleValue('alignSelf', propAlignSelf),
    };
  }, [propAlignSelf]);

  const appleLogoBlack1IconStyle = useMemo(() => {
    return {
      ...getStyleValue('height', propHeight),
    };
  }, [propHeight]);

  const facebook2IconStyle = useMemo(() => {
    return {
      ...getStyleValue('marginLeft', propMarginLeft),
    };
  }, [propMarginLeft]);

  const search2IconStyle = useMemo(() => {
    return {
      ...getStyleValue('height', propHeight1),
      ...getStyleValue('marginLeft', propMarginLeft1),
    };
  }, [propHeight1, propMarginLeft1]);

  return (
    <View style={styles.frameParent}>
      <View style={styles.frameGroupFlexBox}>
        <View style={[styles.lineParent, styles.frameGroupFlexBox]}>
          <View style={styles.frameBorder} />
          <Text style={styles.ngNhpBng}>Đăng nhập bằng</Text>
          <View style={[styles.frameItem, styles.frameBorder]} />
        </View>
        <View style={[styles.appleLogoBlack1Parent, frameViewStyle]}>
          <Image
            style={[styles.appleLogoBlack1Icon, appleLogoBlack1IconStyle]}
            resizeMode="cover"
            source={appleLogoBlack1}
          />
          <Image
            style={[styles.facebook2Icon, facebook2IconStyle]}
            resizeMode="cover"
            source={facebook2}
          />
          <Image
            style={[styles.facebook2Icon, search2IconStyle]}
            resizeMode="cover"
            source={search2}
          />
        </View>
      </View>
      <Pressable
        style={styles.chaCTiKhonNgKNgWrapper}
        onPress={onFramePressablePress}>
        <Text style={styles.chaCTiContainer}>
          <Text style={styles.chaCTi}>{chaCTiKhon}</Text>
          <Text style={styles.ngKNgay}>{ngKNgay}</Text>
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  frameGroupFlexBox: {
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  frameBorder: {
    height: 1,
    borderTopWidth: 0.8,
    borderColor: '#d9d9d9',
    borderStyle: 'solid',
    flex: 1,
  },
  ngNhpBng: {
    fontSize: FontSize.body2_size,
    lineHeight: 12,
    fontFamily: FontFamily.body1,
    color: Color.grey,
    marginLeft: 12,
    textAlign: 'left',
  },
  frameItem: {
    marginLeft: 12,
  },
  lineParent: {
    flexDirection: 'row',
  },
  appleLogoBlack1Icon: {
    height: 34,
    width: 28,
  },
  facebook2Icon: {
    height: 28,
    marginLeft: 28,
    width: 28,
  },
  appleLogoBlack1Parent: {
    justifyContent: 'center',
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  chaCTi: {
    color: Color.white,
  },
  ngKNgay: {
    textDecoration: 'underline',
    color: Color.primary2,
  },
  chaCTiContainer: {
    fontSize: FontSize.size_mini,
    lineHeight: 18,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    opacity: 0.8,
    textAlign: 'left',
  },
  chaCTiKhonNgKNgWrapper: {
    padding: Padding.p_3xs,
    marginTop: 40,
    flexDirection: 'row',
  },
  frameParent: {
    position: 'absolute',
    marginLeft: -171.5,
    top: 537,
    left: '50%',
    width: 343,
    alignItems: 'center',
  },
});

export default FrameComponent3;
