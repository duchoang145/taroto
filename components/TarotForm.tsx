import * as React from 'react';
import {Text, StyleSheet, View, Pressable, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Border, Color, FontFamily, FontSize, Padding} from '../GlobalStyles';

const TarotForm = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.frameParent}>
      <Pressable
        style={[styles.frameWrapper, styles.frameWrapperFlexBox1]}
        onPress={() => navigation.navigate('HcChiTit' as never)}>
        <View style={[styles.frameContainer, styles.frameWrapperFlexBox]}>
          <View
            style={[styles.ngunGcLchSTarotWrapper, styles.frameWrapperFlexBox]}>
            <Text style={styles.ngunGcTypo}>{'Nguồn gốc & Lịch sử tarot'}</Text>
          </View>
        </View>
      </Pressable>
      <Pressable
        style={[styles.framePressable, styles.frameWrapperFlexBox1]}
        onPress={() => navigation.navigate('BBi' as never)}>
        <View style={[styles.frameView, styles.frameWrapperFlexBox]}>
          <View
            style={[styles.iconcaretleftParent, styles.frameWrapperFlexBox]}>
            <Image
              style={styles.iconcaretleft}
              resizeMode="cover"
              source={require('../assets/iconcaretleft2.png')}
            />
            <Text style={[styles.ccChunBi, styles.ngunGcTypo]}>
              Các chuẩn bài tarot
            </Text>
          </View>
        </View>
      </Pressable>
      <View style={styles.frameGroup}>
        <View style={styles.frameWrapperSpaceBlock}>
          <View
            style={[
              styles.tarotRiderWaiteSmithWrapper,
              styles.frameWrapperFlexBox,
            ]}>
            <Text style={styles.ngunGcTypo}>Tarot Rider Waite Smith</Text>
          </View>
        </View>
        <View style={[styles.frameWrapper2, styles.frameWrapperSpaceBlock]}>
          <View
            style={[
              styles.tarotRiderWaiteSmithWrapper,
              styles.frameWrapperFlexBox,
            ]}>
            <Text style={styles.ngunGcTypo}>Tarot de Marseille</Text>
          </View>
        </View>
        <View style={[styles.frameWrapper2, styles.frameWrapperSpaceBlock]}>
          <View
            style={[
              styles.tarotRiderWaiteSmithWrapper,
              styles.frameWrapperFlexBox,
            ]}>
            <Text style={styles.ngunGcTypo}>Thoth Tarot</Text>
          </View>
        </View>
      </View>
      <View style={[styles.frameWrapper4, styles.frameWrapperFlexBox1]}>
        <View style={[styles.frameWrapper5, styles.frameWrapperFlexBox]}>
          <View
            style={[styles.ngunGcLchSTarotWrapper, styles.frameWrapperFlexBox]}>
            <Text style={styles.ngunGcTypo}>Cách trải bài tarot</Text>
          </View>
        </View>
      </View>
      <View style={[styles.frameWrapper4, styles.frameWrapperFlexBox1]}>
        <View style={[styles.frameView, styles.frameWrapperFlexBox]}>
          <View
            style={[styles.ngunGcLchSTarotWrapper, styles.frameWrapperFlexBox]}>
            <Text style={styles.ngunGcTypo}>Cách đặt câu hỏi</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  frameWrapperFlexBox1: {
    alignSelf: 'stretch',
    overflow: 'hidden',
    borderRadius: Border.br_9xs,
  },
  frameWrapperFlexBox: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ngunGcTypo: {
    textAlign: 'left',
    color: Color.secondary2,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    fontSize: FontSize.heading3_size,
  },
  frameWrapperSpaceBlock: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_base,
    backgroundColor: Color.primary6,
    borderRadius: Border.br_9xs,
  },
  ngunGcLchSTarotWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  frameContainer: {
    paddingVertical: Padding.p_xs,
    backgroundColor: Color.primary6,
    alignItems: 'center',
    paddingHorizontal: Padding.p_xl,
    flexDirection: 'row',
    alignSelf: 'stretch',
  },
  frameWrapper: {
    overflow: 'hidden',
    borderRadius: Border.br_9xs,
    justifyContent: 'center',
  },
  iconcaretleft: {
    width: 16,
    height: 16,
    overflow: 'hidden',
  },
  ccChunBi: {
    marginLeft: 8,
  },
  iconcaretleftParent: {
    width: 157,
    flexDirection: 'row',
    alignItems: 'center',
  },
  frameView: {
    borderStyle: 'solid',
    borderColor: '#2e2e30',
    borderWidth: 1,
    paddingVertical: Padding.p_xs,
    backgroundColor: Color.primary6,
    alignItems: 'center',
    paddingHorizontal: Padding.p_xl,
    flexDirection: 'row',
    alignSelf: 'stretch',
    borderRadius: Border.br_9xs,
  },
  framePressable: {
    marginTop: 8,
    overflow: 'hidden',
    borderRadius: Border.br_9xs,
  },
  tarotRiderWaiteSmithWrapper: {
    width: 185,
    flexDirection: 'row',
    alignItems: 'center',
  },
  frameWrapper2: {
    marginTop: 8,
  },
  frameGroup: {
    paddingVertical: 0,
    marginTop: 8,
    paddingHorizontal: Padding.p_xl,
  },
  frameWrapper5: {
    paddingVertical: Padding.p_xs,
    backgroundColor: Color.primary6,
    alignItems: 'center',
    paddingHorizontal: Padding.p_xl,
    flexDirection: 'row',
    alignSelf: 'stretch',
    borderRadius: Border.br_9xs,
  },
  frameWrapper4: {
    marginTop: 8,
    overflow: 'hidden',
    borderRadius: Border.br_9xs,
    justifyContent: 'center',
  },
  frameParent: {
    position: 'absolute',
    marginTop: -224,
    marginLeft: -171.5,
    top: '50%',
    left: '50%',
    width: 343,
    justifyContent: 'center',
  },
});

export default TarotForm;
