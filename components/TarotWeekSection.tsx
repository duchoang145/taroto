import React, {useMemo} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Pressable,
  ImageSourcePropType,
} from 'react-native';
import {Color, FontSize, FontFamily, Border, Padding} from '../GlobalStyles';

type TarotWeekSectionType = {
  tarotId: ImageSourcePropType;
  tarotType: string;
  tarotQuestion: string;

  /** Style props */
  propBackgroundColor?: string;
  propMarginTop?: number | string;

  /** Action props */
  onFramePressablePress?: () => void;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const TarotWeekSection = ({
  tarotId,
  tarotType,
  tarotQuestion,
  propBackgroundColor,
  propMarginTop,
  onFramePressablePress,
}: TarotWeekSectionType) => {
  const framePressableStyle = useMemo(() => {
    return {
      ...getStyleValue('backgroundColor', propBackgroundColor),
      ...getStyleValue('marginTop', propMarginTop),
    };
  }, [propBackgroundColor, propMarginTop]);

  return (
    <Pressable
      style={[styles.tarotCard1Parent, framePressableStyle]}
      onPress={onFramePressablePress}>
      <Image
        style={styles.tarotCard1Icon}
        resizeMode="cover"
        source={tarotId}
      />
      <View style={styles.tarotNgyParent}>
        <Text style={[styles.tarotNgy, styles.ngyFlexBox]}>{tarotType}</Text>
        <Text style={[styles.ngyHmNay, styles.ngyFlexBox]}>
          {tarotQuestion}
        </Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  ngyFlexBox: {
    textAlign: 'left',
    color: Color.secondary2,
  },
  tarotCard1Icon: {
    width: 40,
    height: 40,
  },
  tarotNgy: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
  },
  ngyHmNay: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    marginTop: 4,
  },
  tarotNgyParent: {
    marginLeft: 28,
  },
  tarotCard1Parent: {
    alignSelf: 'stretch',
    borderRadius: Border.br_5xs,
    backgroundColor: Color.primary6,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 40,
    elevation: 40,
    shadowOpacity: 1,
    overflow: 'hidden',
    flexDirection: 'row',
    paddingHorizontal: Padding.p_21xl,
    paddingVertical: Padding.p_xs,
    alignItems: 'center',
  },
});

export default TarotWeekSection;
