import React, {useMemo} from 'react';
import {Image, StyleSheet, View, Text, ImageSourcePropType} from 'react-native';
import {Padding, FontSize, FontFamily, Color, Border} from '../GlobalStyles';

type BackSectionType = {
  trV?: string;
  lock: ImageSourcePropType;

  /** Style props */
  propOpacity?: number | string;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const BackSection = ({trV, lock, propOpacity}: BackSectionType) => {
  const frameView1Style = useMemo(() => {
    return {
      ...getStyleValue('opacity', propOpacity),
    };
  }, [propOpacity]);

  return (
    <View style={[styles.caretleftParent, styles.parentFlexBox]}>
      <View style={styles.caretleft}>
        <Image
          style={styles.vectorIcon}
          resizeMode="cover"
          source={require('../assets/vector.png')}
        />
      </View>
      <Text style={styles.trV}>{trV}</Text>
      <View style={[styles.lockParent, styles.parentFlexBox, frameView1Style]}>
        <Image style={styles.lockIcon} resizeMode="cover" source={lock} />
        <Text style={styles.comingSoon}>Coming soon</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  parentFlexBox: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  vectorIcon: {
    width: 8,
    height: 14,
  },
  caretleft: {
    width: 109,
    paddingHorizontal: Padding.p_7xs,
    paddingVertical: Padding.p_10xs,
    overflow: 'hidden',
    flexDirection: 'row',
  },
  trV: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.primary2,
    textAlign: 'left',
  },
  lockIcon: {
    width: 14,
    height: 14,
    overflow: 'hidden',
  },
  comingSoon: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    color: Color.grey,
    marginLeft: 4,
    textAlign: 'left',
  },
  lockParent: {
    borderRadius: Border.br_9xs,
    backgroundColor: Color.secondary6,
    padding: Padding.p_9xs,
    opacity: 0,
  },
  caretleftParent: {
    position: 'absolute',
    top: 38,
    left: 0,
    width: 375,
    paddingHorizontal: Padding.p_3xs,
    paddingVertical: Padding.p_xs,
    justifyContent: 'space-between',
  },
});

export default BackSection;
