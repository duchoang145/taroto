import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import TarotWeekSection from './TarotWeekSection';
import {Color, Padding, Border, FontSize, FontFamily} from '../GlobalStyles';

const TarotCard = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.frameParent}>
      <TarotWeekSection
        tarotId={require('../assets/tarotcard-1.png')}
        tarotType="Tarot ngày "
        tarotQuestion="Ngày hôm nay của bạn như thế nào"
        propBackgroundColor="#fff7e9"
        onFramePressablePress={() => navigation.navigate('RtBi' as never)}
      />
      <TarotWeekSection
        tarotId={require('../assets/tarot-1.png')}
        tarotType="Tarot tuần"
        tarotQuestion=" tuần này của bạn như thế nào"
        propBackgroundColor="#8e8e8e"
        propMarginTop={20}
        onFramePressablePress={() => navigation.navigate('TarotTun' as never)}
      />
      <View style={styles.frameGroupSpaceBlock}>
        <Image
          style={styles.crystalBall1Icon}
          resizeMode="cover"
          source={require('../assets/crystalball-1.png')}
        />
        <View style={styles.tarotThngParent}>
          <Text style={[styles.tarotThng, styles.thngFlexBox]}>
            {'Tarot tháng '}
          </Text>
          <Text style={[styles.thngNyCa, styles.thngFlexBox]}>
            tháng này của bạn như thế nào
          </Text>
        </View>
      </View>
      <View style={[styles.frameGroup, styles.frameGroupSpaceBlock]}>
        <View style={styles.crystalBall1Group}>
          <Image
            style={styles.crystalBall1Icon}
            resizeMode="cover"
            source={require('../assets/crystalball-11.png')}
          />
          <View style={styles.tarotThngParent}>
            <Text style={[styles.tarotThng, styles.thngFlexBox]}>Câu hỏi</Text>
            <Text style={[styles.thngNyCa, styles.thngFlexBox]}>
              Tháo gỡ những nút thắt
            </Text>
          </View>
        </View>
        <Image
          style={styles.lockIcon}
          resizeMode="cover"
          source={require('../assets/lock1.png')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  thngFlexBox: {
    textAlign: 'left',
    color: Color.secondary2,
  },
  frameGroupSpaceBlock: {
    marginTop: 20,
    paddingVertical: Padding.p_xs,
    paddingHorizontal: Padding.p_21xl,
    backgroundColor: Color.secondary6,
    borderRadius: Border.br_5xs,
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  crystalBall1Icon: {
    width: 40,
    height: 40,
  },
  tarotThng: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
  },
  thngNyCa: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    marginTop: 4,
  },
  tarotThngParent: {
    marginLeft: 28,
  },
  crystalBall1Group: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  lockIcon: {
    width: 32,
    height: 32,
    opacity: 0,
    overflow: 'hidden',
  },
  frameGroup: {
    justifyContent: 'space-between',
  },
  frameParent: {
    position: 'absolute',
    marginTop: -158,
    top: '50%',
    left: 16,
    width: 343,
  },
});

export default TarotCard;
