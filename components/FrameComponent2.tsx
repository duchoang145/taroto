import React, {useMemo} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {Padding, FontSize, FontFamily, Color} from '../GlobalStyles';

type FrameComponent2Type = {
  ngunGcVLchSTarot?: string;

  /** Style props */
  propColor?: string;
  propDisplay?: string;
  propDisplay1?: string;
  propDisplay2?: string;
};

const getStyleValue = (key: string, value: string | number | undefined) => {
  if (value === undefined) {
    return;
  }
  return {[key]: value === 'unset' ? undefined : value};
};
const FrameComponent2 = ({
  ngunGcVLchSTarot,
  propColor,
  propDisplay,
  propDisplay1,
  propDisplay2,
}: FrameComponent2Type) => {
  const ngunGcVStyle = useMemo(() => {
    return {
      ...getStyleValue('color', propColor),
    };
  }, [propColor]);

  const frameView4Style = useMemo(() => {
    return {
      ...getStyleValue('display', propDisplay),
    };
  }, [propDisplay]);

  const frameView5Style = useMemo(() => {
    return {
      ...getStyleValue('display', propDisplay1),
    };
  }, [propDisplay1]);

  const frameView6Style = useMemo(() => {
    return {
      ...getStyleValue('display', propDisplay2),
    };
  }, [propDisplay2]);

  return (
    <View style={styles.frameParent}>
      <View style={styles.wrapperSpaceBlock}>
        <Text style={[styles.ngunGcV, ngunGcVStyle]}>{ngunGcVLchSTarot}</Text>
      </View>
      <View
        style={[
          styles.tnhCmWrapper,
          styles.wrapperSpaceBlock,
          frameView4Style,
        ]}>
        <Text style={styles.ngunGcV}>Tình cảm</Text>
      </View>
      <View
        style={[
          styles.tnhCmWrapper,
          styles.wrapperSpaceBlock,
          frameView5Style,
        ]}>
        <Text style={styles.ngunGcV}>Công việc</Text>
      </View>
      <View
        style={[
          styles.tnhCmWrapper,
          styles.wrapperSpaceBlock,
          frameView6Style,
        ]}>
        <Text style={styles.ngunGcV}>Sức khoẻ</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapperSpaceBlock: {
    paddingVertical: Padding.p_3xs,
    paddingHorizontal: Padding.p_9xs,
    flexDirection: 'row',
  },
  ngunGcV: {
    fontSize: FontSize.heading3_size,
    fontWeight: '500',
    fontFamily: FontFamily.heading3,
    color: Color.white,
    textAlign: 'left',
  },
  tnhCmWrapper: {
    display: 'none',
  },
  frameParent: {
    alignSelf: 'stretch',
    backgroundColor: Color.secondary5,
    paddingHorizontal: Padding.p_xs,
    paddingVertical: Padding.p_9xs,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

export default FrameComponent2;
