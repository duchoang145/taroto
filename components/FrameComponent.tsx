import * as React from 'react';
import {View, Image, StyleSheet} from 'react-native';

type FrameComponentType = {
  onClose?: () => void;
};

const FrameComponent = ({onClose}: FrameComponentType) => {
  return (
    <View style={styles.asset14x2Parent}>
      <Image
        style={styles.asset14x2}
        resizeMode="cover"
        source={require('../assets/asset-14x-29.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  asset14x2: {
    position: 'absolute',
    marginLeft: -212.27,
    top: -113,
    left: '50%',
    width: 483,
    height: 662,
  },
  asset14x2Parent: {
    width: 331,
    height: 467,
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

export default FrameComponent;
