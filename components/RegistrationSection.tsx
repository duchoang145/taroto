import * as React from 'react';
import {Image, StyleSheet, Text, View, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Color, Padding, FontFamily, FontSize, Border} from '../GlobalStyles';

const RegistrationSection = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.frameParent}>
      <View style={styles.frameGroup}>
        <View style={styles.frameContainer}>
          <View style={styles.parentBorder}>
            <Image
              style={styles.envelopeIcon}
              resizeMode="cover"
              source={require('../assets/envelope.png')}
            />
            <Text style={[styles.nhpEmailCa, styles.qunMtKhuClr]}>
              Nhập email của bạn...
            </Text>
          </View>
          <View style={[styles.lockParent, styles.parentBorder]}>
            <Image
              style={styles.envelopeIcon}
              resizeMode="cover"
              source={require('../assets/lock3.png')}
            />
            <Text style={[styles.nhpEmailCa, styles.qunMtKhuClr]}>
              Nhập mật khẩu của bạn ..
            </Text>
          </View>
        </View>
        <Pressable
          style={styles.ngKWrapper}
          onPress={() => navigation.navigate('Home' as never)}>
          <Text style={[styles.ngK, styles.ngKTypo]}>Đăng ký</Text>
        </Pressable>
      </View>
      <Text style={[styles.qunMtKhu, styles.ngKTypo]}>Quên mật khẩu ?</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  qunMtKhuClr: {
    opacity: 0.8,
    color: Color.white,
  },
  parentBorder: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: 0,
    borderBottomWidth: 1,
    borderColor: '#e1bf89',
    borderStyle: 'solid',
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  ngKTypo: {
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    textAlign: 'left',
  },
  envelopeIcon: {
    width: 24,
    height: 24,
    overflow: 'hidden',
  },
  nhpEmailCa: {
    fontSize: FontSize.body1_size,
    lineHeight: 17,
    fontFamily: FontFamily.body1,
    marginLeft: 12,
    textAlign: 'left',
    opacity: 0.8,
    color: Color.white,
  },
  lockParent: {
    marginTop: 28,
  },
  frameContainer: {
    alignSelf: 'stretch',
  },
  ngK: {
    fontSize: FontSize.heading3_size,
    color: Color.secondary1,
  },
  ngKWrapper: {
    borderRadius: Border.br_9xs,
    backgroundColor: Color.primary2,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 30,
    elevation: 30,
    shadowOpacity: 1,
    height: 38,
    paddingHorizontal: Padding.p_base,
    paddingVertical: Padding.p_3xs,
    justifyContent: 'center',
    marginTop: 40,
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  frameGroup: {
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  qunMtKhu: {
    fontSize: FontSize.size_mini,
    textDecoration: 'underline',
    lineHeight: 18,
    marginTop: 28,
    opacity: 0.8,
    color: Color.white,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
  },
  frameParent: {
    position: 'absolute',
    marginLeft: -171.5,
    top: 177,
    left: '50%',
    width: 343,
    alignItems: 'center',
  },
});

export default RegistrationSection;
