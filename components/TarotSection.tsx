import * as React from 'react';
import {Image, StyleSheet, Text, View, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Color, FontFamily, FontSize} from '../GlobalStyles';

const TarotSection = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.frameParent}>
      <View style={styles.frameGroup}>
        <Pressable
          style={styles.tarotParentShadowBox}
          onPress={() => navigation.navigate('Hc' as never)}>
          <Image
            style={styles.tarotCard1Icon}
            resizeMode="cover"
            source={require('../assets/tarotcard-11.png')}
          />
          <Text style={[styles.tarot, styles.tarotTypo]}>Tarot</Text>
        </Pressable>
        <View style={[styles.tarotCard1Group, styles.tarotParentShadowBox]}>
          <Image
            style={styles.tarotCard1Icon}
            resizeMode="cover"
            source={require('../assets/tarotcard-12.png')}
          />
          <Text style={[styles.tarot, styles.tarotTypo]}>12 con giáp</Text>
        </View>
      </View>
      <View style={styles.frameContainer}>
        <View style={styles.tarotParentShadowBox}>
          <Image
            style={styles.tarotCard1Icon}
            resizeMode="cover"
            source={require('../assets/tarotcard-13.png')}
          />
          <Text style={[styles.tarot, styles.tarotTypo]}>Chiêm tinh học</Text>
        </View>
        <View style={styles.tarotParentShadowBox}>
          <Image
            style={styles.tarotCard1Icon}
            resizeMode="cover"
            source={require('../assets/tarotcard-14.png')}
          />
          <Text style={[styles.cungHongO, styles.tarotTypo]}>
            12 cung hoàng đạo
          </Text>
        </View>
        <View style={styles.tarotParentShadowBox}>
          <Image
            style={styles.tarotCard1Icon}
            resizeMode="cover"
            source={require('../assets/tarotcard-15.png')}
          />
          <Text style={[styles.tarot, styles.tarotTypo]}>Thần số học</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tarotTypo: {
    marginTop: 8,
    color: Color.white,
    fontFamily: FontFamily.heading3,
    fontWeight: '500',
    fontSize: FontSize.heading3_size,
  },
  tarotParentShadowBox: {
    shadowOpacity: 1,
    elevation: 20,
    shadowRadius: 20,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    alignItems: 'center',
  },
  tarotCard1Icon: {
    width: 60,
    height: 60,
  },
  tarot: {
    textAlign: 'left',
  },
  tarotCard1Group: {
    marginLeft: 60,
  },
  frameGroup: {
    width: 181,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cungHongO: {
    textAlign: 'center',
    width: 75,
  },
  frameContainer: {
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    marginTop: 30,
    flexDirection: 'row',
  },
  frameParent: {
    position: 'absolute',
    marginTop: -121,
    marginLeft: -171.5,
    top: '50%',
    left: '50%',
    width: 343,
    alignItems: 'center',
  },
});

export default TarotSection;
